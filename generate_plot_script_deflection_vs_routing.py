
import sys 

print "set xrange[5:100]"
print "set yrange[0:100]"
print "set key left"
print "set term png"


xaxis = "traffic_load (%)"
print("set xlabel \""+xaxis+"\"")


measures = [
["drop-ratio","drop_ratio (%)","9","00","300-drop-ratio-comparison-bfSIZE.csv"],
["delay","delay (iterations)","12","01","300-delay-comparison-bfSIZE.csv"],
["delay-unmarked","delay-unmarked (iterations)","13","02","300-delay-unmarked-comparison-bfSIZE.csv"],
["delay-marked","delay-marked (iterations)","14","03","300-delay-marked-comparison-bfSIZE.csv"],
["received-unmarked","fraction of unmarked success (%)","15","04","300-received-unmarked-comparison-bfSIZE.csv"],
["received-marked","fraction of marked success (%)","16","05","300-received-marked-comparison-bfSIZE.csv"]
]

deflection_functions = [
["NAME-deflection.png","random-deflection degree=16","200-random-deflection-bfSIZE.csv","01","2","deflection","500-random-deflection-bfSIZE-pKTOP.csv"],
["NAME-routing.png","random-routing degree=16","200-random-routing-bfSIZE.csv","02","3","routing","500-random-routing-bfSIZE-pKTOP.csv"]
]
print ""

ktop_list = map(lambda x: int(x), sys.argv[1].split(',')[1:])
bs_list = map(lambda x: int(x), sys.argv[2].split(',')[1:])
limit_rate_list = map(lambda x : float(x)/10,range(0,10))


for ktop in ktop_list:
    h=0
    for measure in measures:
        if measure[1].find("success") == -1 and measure[1].find("drop") == -1:
            print("set auto y")
        else:
            print("set yrange [0:100]")
        print("set ylabel \""+measure[1]+"\"")
      
        j=0
        for deflection_function in deflection_functions:
            sys.stdout.write("set output \""+str(ktop)+"_"+measure[3]+"_"+deflection_function[3]+"_"+deflection_function[0].replace("NAME",measure[0])+"\"\n")
            sys.stdout.write("set title \"ktop="+str(ktop)+" "+deflection_function[1]+"\"\n")
            print "plot ",
            for i in bs_list[0:-1]:
                sys.stdout.write("\""+str(ktop)+"/output/"+deflection_function[2].replace("SIZE",str(i))+"\" using 3:"+measure[2]+" with lines title \"bs "+str(i)+"\",\\\n")
            sys.stdout.write("\""+str(ktop)+"/output/"+deflection_function[2].replace("SIZE",str(bs_list[-1]))+"\" using 3:"+measure[2]+" with lines title \"bs "+str(bs_list[-1])+"\"\n")
            print ""
            j+=1
        h+=1
        
    print ""
    print ""
    print ""
    print "set xlabel \"traffic_load (%)\""
    print "set style fill solid border rgb \"black\""
    print "set auto x"
    print "set yrange [0:100]"

    h=0
    for measure in measures:
        if measure[1].find("success") == -1 and measure[1].find("drop") == -1:
            print("set auto y")
        else:
            print("set yrange [0:100]")
        print("set ylabel \""+measure[1]+"\"")
        for i in bs_list:
      
            sys.stdout.write("set output \"comp_"+str(ktop)+"_"+measure[3]+"_"+str(i)+".png\"\n")
            sys.stdout.write("set title \"ktop="+str(ktop)+" buffer_size="+str(i)+"\"\n")
            print "plot ",
            j=0
            for deflection_function in deflection_functions:
                if j< len(deflection_functions)-1:
                    sys.stdout.write("\""+str(ktop)+"/output/"+measure[4].replace("SIZE",str(i))+"\" using 1:"+deflection_function[4]+" with lines title \""+deflection_function[5]+"\",\\\n")
                j+=1
            sys.stdout.write("\""+str(ktop)+"/output/"+measure[4].replace("SIZE",str(i))+"\" using 1:"+deflection_functions[len(deflection_functions)-1][4]+" with lines title \""+deflection_functions[len(deflection_functions)-1][5]+"\"\n")
            print ""

        h+=1
    
for i in bs_list:
    for measure in measures:
        if measure[1].find("success") == -1 and measure[1].find("drop") == -1:
            print("set auto y")
        else:
            print("set yrange [0:100]")
        print("set ylabel \""+measure[1]+"\"")
        for deflection_function in deflection_functions:
            sys.stdout.write("set output \"bf"+str(i)+"_"+measure[3]+"_"+deflection_function[3]+"_"+deflection_function[0].replace("NAME",measure[0])+"\"\n")
            sys.stdout.write("set title \"ktop="+str(ktop)+" "+deflection_function[1]+" buffer-size="+str(i)+"\"\n")
            print "plot ",
            for p in ktop_list[0:-1]:
                sys.stdout.write("\"output/"+deflection_function[6].replace("SIZE",str(i)).replace("KTOP",str(p))+"\" using 3:"+measure[2]+" with lines title \"ktop "+str(p)+"\",\\\n")
            sys.stdout.write("\"output/"+deflection_function[6].replace("SIZE",str(i)).replace("KTOP",str(ktop_list[-1]))+"\" using 3:"+measure[2]+" with lines title \"ktop "+str(ktop_list[-1])+"\"\n")
            print ""
            j+=1
        h+=1
        

cdfMeasure = ["cdf-delay","% of packets with latency lower than","_","00","400-cdf_lrRATE_random_random-FUNCTION_bsSIZE.csv"]
#generate cdf delay graphs
print ('\n')
print("set ylabel \""+measure[1]+"\"")
print("set xlabel \"delay\"")
print("set xrange [0:50]")
for ktop in ktop_list:
    for i in bs_list:
        for lr in limit_rate_list:
            sys.stdout.write("set output \"cdf_"+str(ktop)+"_"+str(i)+"_"+str(lr)+".png\"\n")
            traffic_load=(1-lr)*100
            sys.stdout.write("set title \"ktop="+str(ktop)+" buffer_size="+str(i)+" traffic-load="+str(traffic_load)+"\"\n")
            print "plot ",
            j=0
            for deflection_function in deflection_functions:
                if j< len(deflection_functions)-1:
                    sys.stdout.write("\""+str(ktop)+"/output/"+cdfMeasure[4].replace("SIZE",str(i)).replace("RATE",str(lr)).replace("FUNCTION",deflection_function[5])+"\" using 1:2 with lines title \""+deflection_function[5]+"\",\\\n")
                j+=1
            sys.stdout.write("\""+str(ktop)+"/output/"+cdfMeasure[4].replace("SIZE",str(i)).replace("RATE",str(lr)).replace("FUNCTION",deflection_function[5])+"\" using 1:2 with lines title \""+deflection_functions[len(deflection_functions)-1][5]+"\"\n")
            print ""

        
     


