#!/bin/bash

#usage one argument -> path with csv files

cd $1

# remove wrong newlines
for x in `ls *_clos*csv`; do cat $x | awk 'gsub(/\r/,"\n"){printf $0;next}{print}' > $x.ok; done

# parse files
for i in `seq 1 1 10`; do for x in `ls *os-de*bs$i.*ok`; do cat $x | tail -n +2 | awk '{r=$3;n=(1-r)*100;gsub(/.*/,n,$3);print $0}'; done |  sort -k3n > clos-deflection-bf$i.csv ; done
for i in `seq 1 1 10`; do for x in `ls *os-no*bs$i.*ok`; do cat $x | tail -n +2 | awk '{r=$3;n=(1-r)*100;gsub(/.*/,n,$3);print $0}'; done |  sort -k3n > clos-no-deflection-bf$i.csv ; done
for i in `seq 1 1 10`; do for x in `ls *os-up*bs$i.*ok`; do cat $x | tail -n +2 | awk '{r=$3;n=(1-r)*100;gsub(/.*/,n,$3);print $0}'; done |  sort -k3n > clos-upward-deflection-bf$i.csv ; done
for i in `seq 1 1 10`; do for x in `ls *os-buffer-fa*bs$i.*ok`; do cat $x | tail -n +2 | awk '{r=$3;n=(1-r)*100;gsub(/.*/,n,$3);print $0}'; done |  sort -k3n > clos-buffer-false-deflection-bf$i.csv ; done
for i in `seq 1 1 10`; do for x in `ls *bs$i-no*.*ok`; do cat $x | tail -n +2 | awk '{r=$3;n=(1-r)*100;gsub(/.*/,n,$3);print $0}'; done |  sort -k3n > clos-buffer-false-deflection-bf$i-noreordering.csv ; done

##########################

# join results
for i in `seq 1 1 10`
do
  join -1 3 -2 3 clos-no-deflection-bf$i.csv clos-upward-deflection-bf$i.csv | join - -2 3 clos-deflection-bf$i.csv | join - -2 3 clos-buffer-false-deflection-bf$i.csv | join - -2 3 clos-buffer-false-deflection-bf$i-noreordering.csv | awk 'BEGIN{print "traffic-load no-deflection upward-deflection deflection  priority-deflection priority-deflection-no-reordering"} {print $1" " $9" " $24" " $39 " " $54 " " $69}' > drop-ratio-comparison-bf$i.csv
  join -1 3 -2 3 clos-no-deflection-bf$i.csv clos-upward-deflection-bf$i.csv | join - -2 3 clos-deflection-bf$i.csv | join - -2 3 clos-buffer-false-deflection-bf$i.csv | join - -2 3 clos-buffer-false-deflection-bf$i-noreordering.csv | awk 'BEGIN{print "traffic-load no-deflection upward-deflection deflection  priority-deflection priority-deflection-no-reordering"} {print $1" " $12" " $27" " $42 " " $57 " " $72}' > delay-comparison-bf$i.csv
  join -1 3 -2 3 clos-no-deflection-bf$i.csv clos-upward-deflection-bf$i.csv | join - -2 3 clos-deflection-bf$i.csv | join - -2 3 clos-buffer-false-deflection-bf$i.csv | join - -2 3 clos-buffer-false-deflection-bf$i-noreordering.csv | awk 'BEGIN{print "traffic-load no-deflection upward-deflection deflection  priority-deflection priority-deflection-no-reordering"} {print $1" " $13" " $28" " $43 " " $58 " " $73}' > delay-unmarked-comparison-bf$i.csv
  join -1 3 -2 3 clos-no-deflection-bf$i.csv clos-upward-deflection-bf$i.csv | join - -2 3 clos-deflection-bf$i.csv | join - -2 3 clos-buffer-false-deflection-bf$i.csv | join - -2 3 clos-buffer-false-deflection-bf$i-noreordering.csv | awk 'BEGIN{print "traffic-load no-deflection upward-deflection deflection  priority-deflection priority-deflection-no-reordering"} {print $1" " $14" " $29" " $44 " " $59 " " $74}' > delay-marked-comparison-bf$i.csv
  join -1 3 -2 3 clos-no-deflection-bf$i.csv clos-upward-deflection-bf$i.csv | join - -2 3 clos-deflection-bf$i.csv | join - -2 3 clos-buffer-false-deflection-bf$i.csv | join - -2 3 clos-buffer-false-deflection-bf$i-noreordering.csv | awk 'BEGIN{print "traffic-load no-deflection upward-deflection deflection  priority-deflection priority-deflection-no-reordering"} {print $1" " $15" " $30" " $45 " " $60 " " $75}' > received-unmarked-comparison-bf$i.csv
  join -1 3 -2 3 clos-no-deflection-bf$i.csv clos-upward-deflection-bf$i.csv | join - -2 3 clos-deflection-bf$i.csv | join - -2 3 clos-buffer-false-deflection-bf$i.csv | join - -2 3 clos-buffer-false-deflection-bf$i-noreordering.csv | awk 'BEGIN{print "traffic-load no-deflection upward-deflection deflection  priority-deflection priority-deflection-no-reordering"} {print $1" " $16" " $31" " $46 " " $61 " " $76}' > received-marked-comparison-bf$i.csv
done


cd ..


python generate_plot_script.py > $1/commands.txt

cd $1 

gnuplot < commands.txt

cd ..



