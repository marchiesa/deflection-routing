import model.network as net
import model.switch as switch
import model.switchfactory as sf
import time

''' This class extends a Network class and implements a Clos network
    with 'level' levels and vertex degree = 'degree' '''

class NetworkClos(net.Network):
    
    def __init__(self,numOfLevels,degree,routingPolicy='clos',bufferSize=1):
        #init
        net.Network.__init__(self,bufferSize)
        self.numOfLevels=numOfLevels
        self.degree=degree
        self.routingPolicy=routingPolicy
        self.levelPosition2switch = {} #a map that return a switch given its coordinates
        ''' recursively constructs a Clos network '''
        self.constructNetworkClosRec(numOfLevels, degree, 0)
        print "clos nodes", len(self.nodes)
        self.speakers=filter(lambda x: x.getLevel()==0,self.nodes)
        
    def getRoutingPolicy(self):
        return self.routingPolicy
    

    def addNode(self,node):
        self.nodes.append(node)
        self.levelPosition2switch[(node.level,node.position)]=node
        if node.level == 0:
            self.addSpeaker(node)
        
    ''' ATTENTION: also nodes attributes must be updated. It is not
        done here because of performance issues for Clos Networks '''
    def addEdges(self,lista):
        self.edges.extend(lista)
    
    
    def addNodes(self,nodess):
        self.nodes.extend(nodess)
        for node in nodess:
            self.levelPosition2switch[(node.level,node.position)]=node
            if node.level == 0:
                self.addSpeaker(node)
    
    ''' given a self Clos network with 'levels' levels it construct a '
        levels'+1 Clos network'''
    def constructNetworkClosRec(self,numOfLevels, degree, levels):
        if levels == 0:
            self.addNode(sf.SwitchFactory(self.routingPolicy,self.bufferSize).getNewSwitch(level=0,position=0))
        elif levels+1<numOfLevels:
            #print "level", levels,
            ''' create 'degree' copies of the original Clos network '''
            self.createCopiesOf(degree,degree,levels)
            #print "nodes", self.nodes
            #print "edges", self.edges
            ''' add a new layer and create all the necessary complete bipartite
                subgraphs among vertices in the last two levels '''
            self.addLayer(degree,degree,levels)
            #print "nodes", self.nodes
            #print "edges", self.edges
        else:
            #print "level", levels,
            ''' create 'degree' copies of the original Clos network '''
            self.createCopiesOf(degree,2*degree,levels)
            #print "nodes", self.nodes
            #print "edges", self.edges
            ''' add a new layer and create all the necessary complete bipartite
                subgraphs among vertices in the last two levels '''
            self.addLayer(degree,2*degree,levels)
        ''' recursively iterate if more levels are still needed '''
        if levels+1<numOfLevels:
            self.constructNetworkClosRec(numOfLevels,degree,levels+1)
            
    def addLayer(self,degree,numberOfCopies,levels):
        '''newNodes=[]
        for i in range(0,degree**levels):
            node = sf.SwitchFactory(self.routingPolicy).getNewSwitch(str(levels)+'_'+str(i))
            newNodes.append(node)'''
        #print "creating",degree**levels,"vertices...",
        ''' create a copy of the set of switches in the upper level of the Clos netowork '''
        newNodes = map(lambda x : sf.SwitchFactory(self.routingPolicy,self.bufferSize).getNewSwitch(level=levels,position=x), range(0,(numberOfCopies/degree)*(degree**levels)))
        #print "done"
            
        #print "adding", len(newNodes),"nodesin a lisst with", len(self.nodes)," nodes...",
        self.addNodes(newNodes)
        #print "done"
        #print "nodes", self.nodes
        
        newEdges=[]   
        factor=degree**(levels-1) ### x-size of the original Clos network
        print "factor",factor
        i=0
        #t = time.time()
        #print "computing", factor*degree*degree,"edges",
        ''' create complete bipartite subgraphs between the new layer and the top pre-existing one '''
        for (x,y) in [(x*factor+z,y*factor+z) 
                      for z in range(0,factor) 
                      for x in range(0,numberOfCopies) 
                      for y in range(0,degree)]:
            print "x",x,"y",y, "n1",str(levels-1)+'_'+str(x),"n2",str(levels)+'_'+str(y)
            n1 = self.getNode(levels-1,x)
            n2 = self.getNode(levels,y)
            interface = str(n1.level)+'_'+str(n1.position)+'#'+str(n2.level)+'_'+str(n2.position)
            newEdges.append((n1,n2,interface))
            i=i+1
            n1.addEdge(n2,interface)
            n2.addEdge(n1,interface)
            #if i % 1000 == 0: 
            #    print i, 
        #tf = time.time()-t
        #print "done","time",tf
        
        #print [(x*factor+z,y*factor+z) for z in range(0,factor) for x in range(0,degree) for y in range(0,degree)]
        #newEdges=map(lambda (x,y): (self.getNode(str(levels-1)+'_'+str(x)),self.getNode(str(levels)+'_'+str(y)),self.getNode(str(levels-1)+'_'+str(x)).name+'#'+self.getNode(str(levels)+'_'+str(y)).name),[(x*factor+z,y*factor+z) for z in range(0,factor) for x in range(0,degree) for y in range(0,degree)])
        #t = time.time()
        #print "adding", len(newEdges),"edges in a lisst with", len(self.edges)," edges...",
        self.addEdges(newEdges)
        #tf = time.time()-t
        #print "done","time",tf
        
    ''' creates 'degree' copies of a 'self' Clos network '''
    def createCopiesOf(self,degree,numberOfCopies,levels):
        newNodes = []
        for i in range(1,numberOfCopies):
            for originalNode in self.nodes:
                node = sf.SwitchFactory(self.routingPolicy,self.bufferSize).getCopySwitch(originalNode)
                node.position=originalNode.position+i*(degree**(levels-1))
                #node.name=originalNode.name.split('_')[0]+'_'+str(int(originalNode.name.split('_')[1])+i*(degree**(levels-1)))
                newNodes.append(node)
            #print i,        
        self.addNodes(newNodes)
        newEdges =[] 
        #t = time.time()
        factors = [x*(degree**(levels-1)) for x in range(1,numberOfCopies)]
        #print "creating copies (edges)...",
        for (n1,n2,a) in self.edges:
            for i in range(1,numberOfCopies):
                nn1=self.getNode(n1.level,n1.position+(factors[i-1]))
                #nn1=self.getNode(n1.name.split('_')[0]+'_'+str(int(n1.name.split('_')[1])+i*(degree**(levels-1))))
                nn2=self.getNode(n2.level,n2.position+(factors[i-1]))
                #nn2=self.getNode(n2.name.split('_')[0]+'_'+str(int(n2.name.split('_')[1])+i*(degree**(levels-1))))
                #print "n1",n1,"n2",n2,"nn1",nn1,"nn2",nn2
                interface = str(nn1.level)+'_'+str(nn1.position)+'#'+str(nn2.level)+'_'+str(nn2.position)
                newEdges.append((nn1,nn2,interface))
                nn1.addEdge(nn2,interface)
                nn2.addEdge(nn1,interface)
            #print i,
        #tf = time.time()-t
        #print "done","time",tf
        
        #t = time.time()
        #print "adding", len(newEdges),"edges in a lisst with", len(self.edges)," edges...",
        self.addEdges(newEdges)
        #tf = time.time()-t
        #print "done","time",tf
        
    def getNode(self,level,position):
        return self.levelPosition2switch.get((level,position))
    
    