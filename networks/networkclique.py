from model import network as net
from model import switch
from model import switchfactory as sf

''' Models a network clique with vertices 1,2,3,4,5,6, and 7 where 1,3, and 4 are speakers '''
class NetworkClique(net.Network):
    
    def __init__(self,routingPolicy,bufferSize=1):
        net.Network.__init__(self,bufferSize)
        self.addNodes([sf.SwitchFactory(routingPolicy).getNewSwitch(x) for x in range(1,8)])
        self.addEdges([(x,y,str(x.__repr__())+str(y.__repr__())) for x in self.getNodes() for y in self.getNodes() if not x.__eq__(y)])
    
    def getSpeakers(self):
        return filter(lambda x : x.name.__eq__('1') or x.name.__eq__('3') or x.name.__eq__('4'), self.getNodes())
    