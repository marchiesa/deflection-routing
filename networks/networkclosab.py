import model.network as net
import model.switch as switch
import model.switchfactory as sf
import time

''' This class extends a Network class and implements an AB Clos network
    with 'level' levels and vertex degree = 'degree' '''

class NetworkClosAB(net.Network):
    
    def __init__(self,numOfLevels,degree,routingPolicy='clos',bufferSize=1):
        #init
        net.Network.__init__(self,bufferSize)
        self.numOfLevels=numOfLevels
        self.degree=degree
        self.routingPolicy=routingPolicy
        self.levelPosition2switch = {} #a map that return a switch given its coordinates
        ''' recursively constructs a Clos network '''
        self.constructNetworkClosRec(numOfLevels, degree, 0)
        
        
    def getRoutingPolicy(self):
        return self.routingPolicy
    
    def addNode(self,node):
        self.nodes.append(node)
        self.levelPosition2switch[(node.level,node.position)]=node
        if node.level == 0:
            self.addSpeaker(node)
        
    ''' ATTENTION: also nodes attributes must be updated. It is not
        done here because of performance issues for Clos Networks '''
    def addEdges(self,lista):
        self.edges.extend(lista)
    
    
    def addNodes(self,nodess):
        self.nodes.extend(nodess)
        for node in nodess:
            self.levelPosition2switch[(node.level,node.position)]=node
            if node.level == 0:
                self.addSpeaker(node)
                
    ''' given a self Clos network with 'levels' levels it construct a '
        levels'+1 Clos network'''
    def constructNetworkClosRec(self,numOfLevels, degree, levels):
        if levels == 0:
            self.addNode(sf.SwitchFactory(self.routingPolicy,self.bufferSize).getNewSwitch(level=0,position=0))
        else:
            #print "level", levels,
            ''' create 'degree' copies of the original Clos network '''
            self.createCopiesOf(degree,levels)
            #print "nodes", self.nodes
            #print "edges", self.edges
            ''' add a new layer and create all the necessary complete bipartite
                subgraphs among vertices in the last two levels '''
            self.addLayer(degree,levels)
            #print "nodes", self.nodes
            #print "edges", self.edges
        ''' recursively iterate if more levels are still needed '''
        if levels+1<numOfLevels:
            self.constructNetworkClosRec(numOfLevels,degree,levels+1)
            
    def addLayer(self,degree,levels):
        '''newNodes=[]
        for i in range(0,degree**levels):
            node = sf.SwitchFactory(self.routingPolicy).getNewSwitch(str(levels)+'_'+str(i))
            newNodes.append(node)'''
        #print "creating",degree**levels,"vertices...",
        ''' create a copy of the set of switches in the upper level of the Clos network '''
        newNodes = map(lambda x : sf.SwitchFactory(self.routingPolicy,self.bufferSize).getNewSwitch(level=levels,position=x), range(0,degree**levels))
        #print "done"
            
        #print "adding", len(newNodes),"nodesin a lisst with", len(self.nodes)," nodes...",
        self.addNodes(newNodes)
        #print "done"
        #print "nodes", self.nodes
        
        newEdges=[]   
        factor=degree**(levels-1) ### x-size of the original Clos network
        i=0
        #t = time.time()
        #print "computing", factor*degree*degree,"edges",
        ''' create two different complete bipartite subgraphs between the new layer and the top pre-existing one '''
        ''' creates A connection. Example: if n=5 and we are adding the 3rd level, then
            it creates 
            [( 0, 0), ( 0, 1), ( 0, 2), ( 0, 3), ( 0, 4), 
             (10, 0), (10, 1), (10, 2), (10, 3), (10, 4), 
             (20, 0), (20, 1), (20, 2), (20, 3), (20, 4), 
             ( 1, 5), ( 1, 6), ( 1, 7), ( 1, 8), ( 1, 9), 
             (11, 5), (11, 6), (11, 7), (11, 8), (11, 9), 
             (21, 5), (21, 6), (21, 7), (21, 8), (21, 9), 
             ( 2,10), ( 2,11), ( 2,12), ( 2,13), ( 2,14), 
             (12,10), (12,11), (12,12), (12,13), (12,14), 
             (22,10), (22,11), (22,12), (22,13), (22,14), 
             ( 3,15), ( 3,16), ( 3,17), ( 3,18), ( 3,19), 
             (13,15), (13,16), (13,17), (13,18), (13,19), 
             (23,15), (23,16), (23,17), (23,18), (23,19), 
             ( 4,20), ( 4,21), ( 4,22), ( 4,23), ( 4,24), 
             (14,20), (14,21), (14,22), (14,23), (14,24), 
             (24,20), (24,21), (24,22), (24,23), (24,24)]'''
        for (x,y) in [(2*x*factor+z,z*degree+y) for z in range(0,factor) for x in range(0,(degree+1)/2) for y in range(0,degree)]:
            #print "x",x,"y",y, "n1",str(levels-1)+'_'+str(x),"n2",str(levels)+'_'+str(y)
            n1 = self.getNode(levels-1,x)
            n2 = self.getNode(levels,y)
            interface = str(n1.level)+'_'+str(n1.position)+'#'+str(n2.level)+'_'+str(n2.position)
            newEdges.append((n1,n2,interface))
            i=i+1
            n1.addEdge(n2,interface)
            n2.addEdge(n1,interface)
            
        ''' create B connections. Example: if n=5 and we are adding the 3rd level, then
            it creates  
            [( 5, 0), ( 5, 5), ( 5, 10), ( 5, 15), ( 5, 20), 
             (15, 0), (15, 5), (15, 10), (15, 15), (15, 20), 
             ( 6, 1), ( 6, 6), ( 6, 11), ( 6, 16), ( 6, 21), 
             (16, 1), (16, 6), (16, 11), (16, 16), (16, 21), 
             ( 7, 2), ( 7, 7), ( 7, 12), ( 7, 17), ( 7, 22), 
             (17, 2), (17, 7), (17, 12), (17, 17), (17, 22), 
             ( 8, 3), ( 8, 8), ( 8, 13), ( 8, 18), ( 8, 23), 
             (18, 3), (18, 8), (18, 13), (18, 18), (18, 23), 
             ( 9, 4), ( 9, 9), ( 9, 14), ( 9, 19), ( 9, 24), 
             (19, 4), (19, 9), (19, 14), (19, 19), (19, 24)]'''
        for (x,y) in [((2*x+1)*factor+z,y*factor+z) for z in range(0,factor) for x in range(0,degree/2) for y in range(0,degree)]:
            #print "x",x,"y",y, "n1",str(levels-1)+'_'+str(x),"n2",str(levels)+'_'+str(y)
            n1 = self.getNode(levels-1,x)
            n2 = self.getNode(levels,y)
            interface = str(n1.level)+'_'+str(n1.position)+'#'+str(n2.level)+'_'+str(n2.position)
            newEdges.append((n1,n2,interface))
            i=i+1
            n1.addEdge(n2,interface)
            n2.addEdge(n1,interface)
        
            #if i % 1000 == 0: 
            #    print i, 
        #tf = time.time()-t
        #print "done","time",tf
        
        #print [(x*factor+z,y*factor+z) for z in range(0,factor) for x in range(0,degree) for y in range(0,degree)]
        #newEdges=map(lambda (x,y): (self.getNode(str(levels-1)+'_'+str(x)),self.getNode(str(levels)+'_'+str(y)),self.getNode(str(levels-1)+'_'+str(x)).name+'#'+self.getNode(str(levels)+'_'+str(y)).name),[(x*factor+z,y*factor+z) for z in range(0,factor) for x in range(0,degree) for y in range(0,degree)])
        #t = time.time()
        #print "adding", len(newEdges),"edges in a lisst with", len(self.edges)," edges...",
        self.addEdges(newEdges)
        #tf = time.time()-t
        #print "done","time",tf
        
    ''' creates 'degree' copies of a 'self' Clos network '''
    def createCopiesOf(self,degree,levels):
        newNodes = []
        for i in range(1,degree):
            for originalNode in self.nodes:
                node = sf.SwitchFactory(self.routingPolicy,self.bufferSize).getCopySwitch(originalNode)
                node.position=originalNode.position+i*(degree**(levels-1))
                #node.name=originalNode.name.split('_')[0]+'_'+str(int(originalNode.name.split('_')[1])+i*(degree**(levels-1)))
                newNodes.append(node)
            #print i,        
        self.addNodes(newNodes)
        newEdges =[] 
        #t = time.time()
        factors = [x*(degree**(levels-1)) for x in range(1,degree)]
        #print "creating copies (edges)...",
        for (n1,n2,a) in self.edges:
            for i in range(1,degree):
                nn1=self.getNode(n1.level,n1.position+(factors[i-1]))
                #nn1=self.getNode(n1.name.split('_')[0]+'_'+str(int(n1.name.split('_')[1])+i*(degree**(levels-1))))
                nn2=self.getNode(n2.level,n2.position+(factors[i-1]))
                #nn2=self.getNode(n2.name.split('_')[0]+'_'+str(int(n2.name.split('_')[1])+i*(degree**(levels-1))))
                #print "n1",n1,"n2",n2,"nn1",nn1,"nn2",nn2
                interface = str(nn1.level)+'_'+str(nn1.position)+'#'+str(nn2.level)+'_'+str(nn2.position)
                newEdges.append((nn1,nn2,interface))
                nn1.addEdge(nn2,interface)
                nn2.addEdge(nn1,interface)
            #print i,
        #tf = time.time()-t
        #print "done","time",tf
        
        #t = time.time()
        #print "adding", len(newEdges),"edges in a lisst with", len(self.edges)," edges...",
        self.addEdges(newEdges)
        #tf = time.time()-t
        #print "done","time",tf
        
    def getNode(self,level,position):
        return self.levelPosition2switch.get((level,position))
    