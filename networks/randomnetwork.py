import model.network as net
import model.switch as switch
import model.switchfactory as sf
import time
import random
#import _kspmodule as ksp
#import _ksptmodule as bestports 

''' This class extends a Network class and implements a random Graph
    according to the defintion in the Jellyfish paper '''

class RandomNetwork(net.Network):
    
    def __init__(self,n,k,r,p,routingPolicy='random-shortest-path',bufferSize=1,fileName=None):
        #init
        net.Network.__init__(self,bufferSize)
        if fileName == None:
            self.n=n
            self.k=k
            self.r=r
            print str(n) + " " + str(k) + " " + str(r)
            self.routingPolicy=routingPolicy
            self.bufferSize=bufferSize
            ''' recursively constructs a Clos network '''
            start = time.time()
            #print "time start",time.time()
            #self.constructRandomNetwork(n,k,r)
            graph_file_name = "graphs/graph_" + str(n) + "_" + str(r) + "_1.txt"
            self.loadNetworks(n, k, r, graph_file_name)
            print "#nodes",len(self.getNodes())
            #print "time constructed network",time.time()-start
            #self.constructRoutingScheme(self.constructRoutingTableKShortestPaths(12))
            #measureStart = time.time()
            path_file_name = "graphs/path_" + str(n) + "_" + str(r) + "_1.txt"
            self.constructRoutingScheme(self.loadRoutingTableKShortestPaths(p,path_file_name))
            print "best ports:" + str(p)
            #self.constructRoutingScheme(self.constructRoutingTableAllShortestPaths())

            #timeElasped = time.time() - measureStart
            #print "constructRouting:" + str(timeElasped) + "s"
        else:
            self.importFromFile(fileName)
        
        allNodes=self.nodes[:]
        numberOfSpeakers = len(self.nodes)/3

        for i in range(0,numberOfSpeakers):
            #randomNode = random.choice(allNodes)
            #allNodes.remove(randomNode)
            randomNode = allNodes[i]
            self.addSpeaker(randomNode)
            
        print "edges", len(self.getEdges())
        
        
    def getRoutingPolicy(self):
        return self.routingPolicy
    
    def getSpeakers(self):
        return self.speakers
    
    def addNode(self,node):
        self.nodes.append(node)
        self.levelPosition2switch[(node.level,node.position)]=node
        
   
    def addNodes(self,nodess):
        self.nodes.extend(nodess)
        for node in nodess:
            self.levelPosition2switch[(node.level,node.position)]=node
            
    def loadNetworks(self,n,k,r,filename):
        f = open(filename, 'r')
        data = f.readline()
        data = f.readline()
        self.nodes= map(lambda x : sf.SwitchFactory(self.routingPolicy,self.bufferSize).getNewSwitch(str(x), self.bufferSize),range(0,n))
        count = 1
        while data:
            srcName = data.split(' ')[0];
            dstName = data.split(' ')[1];
            data = f.readline()
            data = f.readline()
            self.addEdge(self.getNode(srcName), self.getNode(dstName), srcName+"#"+dstName+"_"+str(count))
            count += 1
        f.close()
                
        
    def constructRandomNetwork(self,n,k,r):
        sf.SwitchFactory
        ##print map(lambda x : sf.SwitchFactory(self.routingPolicy,self.bufferSize).getNewSwitch(str(x), self.bufferSize),range(0,n))
        self.nodes= map(lambda x : sf.SwitchFactory(self.routingPolicy,self.bufferSize).getNewSwitch(str(x), self.bufferSize),range(0,n))
        
        i=0
        analyzedVertices=[]
        edges=[]
        l=0
        for vertex in self.nodes:
            #print "edges",edges
            #print "i",i,"n",len(self.nodes), "r",r
            #print "vertices", analyzedVertices
            if i<=r:
                analyzedVertices.append(vertex)
            if i==r:
                for (v1,v2) in [(v1,v2) for v1 in analyzedVertices for v2 in analyzedVertices if v1 < v2]:
                    self.addEdge(v1,v2,v1.getName()+"#"+v2.getName()+"_"+str(l))
                    edges.append((v1,v2,v1.getName()+"#"+v2.getName()+"_"+str(l)))
                    l+=1
            if i>r:
                newEdges=[]
                incidentVerticesAlreadyAdded=[]
                for j in range(0,r/2):
                    
                    #print edges
                    randomEdge=random.choice(edges)
                                        
                    while randomEdge[0] in incidentVerticesAlreadyAdded or  randomEdge[1] in incidentVerticesAlreadyAdded:
                        #print incidentVerticesAlreadyAdded
                        #print "random",randomEdge
                        randomEdge=random.choice(edges)
                    
                    incidentVerticesAlreadyAdded.append(randomEdge[0])
                    incidentVerticesAlreadyAdded.append(randomEdge[1])
                    edges.remove(randomEdge)
                    (v1,v2,a)=randomEdge
                    #remove edge
                    ##print "removing edge", (v1,v2,a)
                    
                    self.removeEdge(v1, v2, a)
                    #add edges       
                    ##print "adding edge", (vertex,v1,vertex.getName()+"#"+v1.getName()+"_"+str(l))             
                    self.addEdge(v1,vertex,v1.getName()+"#"+vertex.getName()+"_"+str(l))
                    newEdges.append((v1,vertex,v1.getName()+"#"+vertex.getName()+"_"+str(l)))
                    l+=1
                    ##print "adding edge", (vertex,v2,vertex.getName()+"#"+v2.getName()+"_"+str(l))
                    self.addEdge(v2,vertex,v2.getName()+"#"+vertex.getName()+"_"+str(l))
                    newEdges.append((v2,vertex,v2.getName()+"#"+vertex.getName()+"_"+str(l)))
                    l+=1
                    
                    
                edges.extend(newEdges)
            i+=1
            
    

    def getNode(self,name):
        return filter(lambda x: x.getName().__eq__(name),self.nodes)[0]
    
    def getNumberOfEdgesBetweenTwoSwitches(self,s1,s2):
        return len(filter(lambda (v1,v2,a): (v1.__eq__(s1) and v2.__eq__(s2)) or (v1.__eq__(s2) and v2.__eq__(s1)),self.edges))
    
    def constructRoutingScheme(self,destination2vertex2link):
        print "start constructRoutingScheme"
        startTime = time.time()
        i=0
        for destination in destination2vertex2link.keys():
            print "i",i,"n",len(destination2vertex2link.keys())
        count = 0
        for destination in destination2vertex2link.keys():
            count += 1
            print "constructRoutingScheme: " + str(count) + " / "+ str(len(destination2vertex2link.keys()))
            for vertex in destination2vertex2link[destination].keys():
                for edge in destination2vertex2link[destination][vertex]:
                    self.getNode(str(vertex)).addOutgoingLinkForDestination(edge,self.getNode(str(destination)))
            i+=1
        print "finish constructRoutingScheme" + str(time.time() - startTime)
        
    def constructRoutingTable(self):
        nodes = self.getNodes()
        destination2vertex2link = {
                                   }
        for dist in nodes:
            distName = dist.getName()
            destination2vertex2link[distName] = {}
            unconnectedNodes = []
            for node in nodes:
                unconnectedNodes.append(node.getName())
            unconnectedNodes.remove(distName)
            
            queue = []
            queue.append(distName)
            while queue:
                nodeName = queue.pop(0)
                edges = [i for i in self.getEdges() if i[0].getName() == nodeName and i[1].getName() in unconnectedNodes]
                for edge in edges:
                    unconnectedNodes.remove(edge[1].getName())
                    queue.append(edge[1].getName())
                    destination2vertex2link[distName][edge[1].getName()] = self.getNode(edge[1].getName()).getBufferTo(self.getNode(nodeName))
                
                edges = [i for i in self.getEdges() if i[1].getName() == nodeName and i[0].getName() in unconnectedNodes]
                for edge in edges:
                    unconnectedNodes.remove(edge[0].getName())
                    queue.append(edge[0].getName())
                    destination2vertex2link[distName][edge[0].getName()] = self.getNode(edge[0].getName()).getBufferTo(self.getNode(nodeName))
        
        return destination2vertex2link
    
    def constructRoutingTableAllShortestPaths(self):
        print "start shortest path algorithm"
        nodes = self.getNodes()
        destination2vertex2link = {}
        
        y=0
        for dist in nodes:
            print "i",y,"n",len(self.nodes)
            y+=1
        dist_count = 0
        #j=60
        for dist in nodes:
            #if j == 0:
            #    break
            #j-=1
            
            print "shortest path: node " + str(dist_count) + "/" + str(len(nodes))
            dist_count += 1
            distName = dist.getName()
            destination2vertex2link[distName] = {}
            unconnectedNodes = []
            node2distance={}
            for node in nodes:
                unconnectedNodes.append(node.getName())
                node2distance[node.getName()]=10000000
            unconnectedNodes.remove(distName)
            
            queue = []
            queue.append(distName)
            node2distance[distName]=0
            while queue:
                nodeName = queue.pop(0)
                distance=node2distance[nodeName]
                edges = [i for i in self.getEdges() if i[0].getName() == nodeName and i[1].getName() in filter(lambda x : node2distance[x]== distance+1 or  node2distance[x]== 10000000,map(lambda x: x.getName(),nodes))]
                for edge in edges:
                    node2distance[edge[1].getName()]=distance+1
                    if edge[1].getName() in unconnectedNodes:
                        unconnectedNodes.remove(edge[1].getName())
                        queue.append(edge[1].getName())
                    if not edge[1].getName() in destination2vertex2link[distName].keys():
                        destination2vertex2link[distName][edge[1].getName()]=[]
                    destination2vertex2link[distName][edge[1].getName()].append(self.getNode(edge[1].getName()).getBufferTo(self.getNode(nodeName)))
                    print "buffer", self.getNode(edge[1].getName()).getBufferTo(self.getNode(nodeName))
                    
                
                edges = [i for i in self.getEdges() if i[1].getName() == nodeName and i[0].getName() in filter(lambda x : node2distance[x]== distance+1 or  node2distance[x]== 10000000,map(lambda x: x.getName(),nodes))]
                for edge in edges:
                    node2distance[edge[0].getName()]=distance+1
                    if edge[0].getName() in unconnectedNodes:
                        unconnectedNodes.remove(edge[0].getName())
                        queue.append(edge[0].getName())
                    if not edge[0].getName() in destination2vertex2link[distName].keys():
                        destination2vertex2link[distName][edge[0].getName()]=[]
                    destination2vertex2link[distName][edge[0].getName()].append(self.getNode(edge[0].getName()).getBufferTo(self.getNode(nodeName)))
                    print "buffer", self.getNode(edge[0].getName()).getBufferTo(self.getNode(nodeName))
                    
        for dist in nodes:
            distName = dist.getName()
            for src in nodes:
                srcName = src.getName()
                if srcName == distName:
                    destination2vertex2link[distName][srcName] = []
                else:
                    destination2vertex2link[distName][srcName] = list(set(destination2vertex2link[distName][srcName]))
        print "end shortest path algorithm"
        return destination2vertex2link
                    
    def constructRoutingTableAllShortestPaths_C(self):
        nodes = self.getNodes()
        graph = "" + str(len(nodes)) + "\n"
        for edge in self.getEdges():
            graph += edge[0].getName() + " " + edge[1].getName() + " 1\n"
            graph += edge[1].getName() + " " + edge[0].getName() + " 1\n"
        print "start compute"
        startTime = time.time()
        kspResults = None 
        #ksp.ksp(graph,  12) #2^31-1
        #kspResults = ksp.dsp(graph) #2^31-1
        print "elapsed" + str(time.time() - startTime)
        destination2vertex2link = {}
        
        for dist in nodes:
            distName = dist.getName()
            destination2vertex2link[distName] = {}
            for src in nodes:
                srcName = src.getName()
                destination2vertex2link[distName][srcName] = []
                
        for pair in kspResults.split(';')[1:-1]:
            ##print "pair:" + pair
            srcName = pair.split('\n')[0].split(',')[0]
            distName = pair.split('\n')[0].split(',')[1]            
            for route in pair.split('\n')[1:-1]:
                if route.split(',')[0] > pair.split('\n')[1].split(',')[0]:
                    break
                throughName = route.split(',')[2].split('->')[1]
                destination2vertex2link[distName][srcName].append(self.getNode(srcName).getBufferTo(self.getNode(throughName)))
                destination2vertex2link[distName][srcName] = list(set(destination2vertex2link[distName][srcName]))
            #fix a weird problem of 0,1
            if srcName == '1' and distName == '0':
                for route in pair.split('\n')[1:-1]:
                    if route.split(',')[0] > pair.split('\n')[1].split(',')[0]:
                        break
                    throughName = route.split(',')[2].split('->')[-3]
                    destination2vertex2link[srcName][distName].append(self.getNode(distName).getBufferTo(self.getNode(throughName)))
                    destination2vertex2link[srcName][distName] = list(set(destination2vertex2link[srcName][distName]))
        #print destination2vertex2link
        return destination2vertex2link
    
    def loadRoutingTableKShortestPaths(self, top_k, filename):
        startTime = time.time()
        print "start load routing"
        destination2vertex2link = {}
        nodes = self.getNodes()
        for dist in nodes:
            distName = dist.getName()
            destination2vertex2link[distName] = {}
            for src in nodes:
                srcName = src.getName()
                destination2vertex2link[distName][srcName] = []
        f = open(filename, 'r')
        data = f.readline()
        entry = 1
        while data:
            entry += 1
            if entry % 1000 == 0:
                print str(entry/1000)
            distName = data.split(";")[0].split(",")[0] 
            srcName = data.split(";")[0].split(",")[1]
            count = 1
            while True and count <= top_k:
                try:
                    throughName = data.split(";")[count].split(",")[0]
                    count += 1
                    destination2vertex2link[distName][srcName].append(self.getNode(srcName).getBufferTo(self.getNode(throughName)))
                except:
                    break;
            data = f.readline()
        f.close()
        print "finish load routing" + str(time.time() - startTime)
        return destination2vertex2link
        
    def constructRoutingTableKShortestPaths(self, top_k):
        nodes = self.getNodes()
        graph = "" + str(len(nodes)) + "\n"
        for edge in self.getEdges():
            graph += edge[0].getName() + " " + edge[1].getName() + " 1\n"
            graph += edge[1].getName() + " " + edge[0].getName() + " 1\n"

        #f = open('graphs/graph1.txt', 'r')
        #graph = f.read()
        kspResults = None
        # bestports.best_k_ports(graph, top_k)


        
        destination2vertex2link = {}
        
        for dist in nodes:
            distName = dist.getName()
            destination2vertex2link[distName] = {}
            for src in nodes:
                srcName = src.getName()
                destination2vertex2link[distName][srcName] = []
                
        for pair in kspResults.split(';')[1:-1]:
            ##print "pair:" + pair
            srcName = pair.split('\n')[0].split(',')[0]
            distName = pair.split('\n')[0].split(',')[1]            
            for route in pair.split('\n')[1:-1]:
                throughName = route.split(',')[2].split('->')[1]
                destination2vertex2link[distName][srcName].append(self.getNode(srcName).getBufferTo(self.getNode(throughName)))
            #fix a weird problem of 0,1
            if srcName == '1' and distName == '0':
                for route in pair.split('\n')[1:-1]:
                    throughName = route.split(',')[2].split('->')[-3]
                    destination2vertex2link[srcName][distName].append(self.getNode(distName).getBufferTo(self.getNode(throughName)))
        #print destination2vertex2link
        for dist in nodes:
            distName = dist.getName()
            for src in nodes:
                srcName = src.getName()
                if srcName == distName:
                    destination2vertex2link[distName][srcName] = []
                else:
                    destination2vertex2link[distName][srcName] = list(set(destination2vertex2link[distName][srcName]))
        print "end shortest path algorithm"
        return destination2vertex2link
    
    def exportToFile(self,fileName):
        import csv
        with open(fileName, 'wb') as csvfile:
            writer = csv.writer(csvfile, delimiter=' ',quotechar='|', quoting=csv.QUOTE_MINIMAL)
            
            writer.writerow([len(self.edges)])
            writer.writerow([]) 
            
            for edge in self.edges:
                writer.writerow([edge[0],edge[1],1])
        
        
        #TODO
    def importRandomNetworkFromFile(self,fileName):
        import csv
        i=0
        #create vertices
        with open(fileName, 'rb') as f:
            reader = csv.reader(f)
            for row in reader:
                if i>=2:
                    (v1,v2,w)=row.split(' ')
                    if len(filter(lambda x : x.getName().__eq__(v1),self.nodes)) == 0:
                        s=sf.SwitchFactory(self.routingPolicy,self.bufferSize).getNewSwitch(str(v1), self.bufferSize)
                        self.nodes.append(s)
                    if len(filter(lambda x : x.getName().__eq__(v2),self.nodes)) == 0:
                        s=sf.SwitchFactory(self.routingPolicy,self.bufferSize).getNewSwitch(str(v2), self.bufferSize)
                        self.nodes.append(s)
                    #print row
                i+=1
        
        with open(fileName, 'rb') as f:
            reader = csv.reader(f)
            for row in reader:
                if i>=2:
                    (v1,v2,w)=row.split(' ')
                    s1= self.getNode(v1)
                    s2= self.getNode(v2)
                    self.addEdge(s1, s2, s1.getName()+"#"+s2.getName())
                    #print row
                i+=1
        
        
        
#network = RandomNetwork(432,0,16)
