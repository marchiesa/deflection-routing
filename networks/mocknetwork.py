import model.network as net
import model.switch as switch
import model.switchfactory as sf
import time
import random
#import _kspmodule as ksp 

''' This class extends a Network class and implements a Mock Network
    as follows

            +--+              +--+
   5--------| 0+--------------+ 1|
            +-++              +-++
              |          ____/  |
              |         /       |
              |       /         |
              |     /           |
              |   /             |
              | /               |
            +-++              +-++
            | 2+--------------+ 3|
            +-++              +-++
              |                 |
              |                 |
              |                 |
              |     +--+        |
              +-----+ 4+--------+
                    +--+'''

class MockNetwork(net.Network):
    
    def __init__(self,routingPolicy='random-shortest-path',bufferSize=1):
        #init
        net.Network.__init__(self,bufferSize)
        self.routingPolicy=routingPolicy
        self.bufferSize=bufferSize
        ''' recursively constructs a Clos network '''
        self.constructMockNetwork()
    
        self.constructRoutingScheme(self.constructRoutingTableAllShortestPaths())
        #self.constructRoutingScheme(self.constructRoutingTableKShortestPaths(10))
        
        
    def getRoutingPolicy(self):
        return self.routingPolicy
    
    def getSpeakers(self):
        return self.getNode("1")
    
    def addNode(self,node):
        self.nodes.append(node)
        self.levelPosition2switch[(node.level,node.position)]=node
        
    ''' ATTENTION: also nodes attributes must be updated. It is not
        done here because of performance issues for Clos Networks '''
    def addEdges(self,lista):
        self.edges.extend(lista)
    
    
    def addNodes(self,nodess):
        self.nodes.extend(nodess)
        for node in nodess:
            self.levelPosition2switch[(node.level,node.position)]=node
    
    def constructMockNetwork(self):
        self.nodes= map(lambda x : sf.SwitchFactory(self.routingPolicy,self.bufferSize).getNewSwitch(str(x), self.bufferSize),range(0,6))
        
        self.addEdge(self.nodes[0],self.nodes[1],self.nodes[0].getName()+"#"+self.nodes[1].getName())
        self.addEdge(self.nodes[0],self.nodes[2],self.nodes[0].getName()+"#"+self.nodes[2].getName())
        self.addEdge(self.nodes[3],self.nodes[2],self.nodes[3].getName()+"#"+self.nodes[2].getName())
        self.addEdge(self.nodes[4],self.nodes[2],self.nodes[4].getName()+"#"+self.nodes[2].getName())
        self.addEdge(self.nodes[3],self.nodes[1],self.nodes[3].getName()+"#"+self.nodes[1].getName())
        self.addEdge(self.nodes[3],self.nodes[4],self.nodes[3].getName()+"#"+self.nodes[4].getName())
        self.addEdge(self.nodes[0],self.nodes[5],self.nodes[0].getName()+"#"+self.nodes[5].getName())
        self.addEdge(self.nodes[1],self.nodes[2],self.nodes[1].getName()+"#"+self.nodes[2].getName())
        
    def constructRoutingTable(self):
        nodes = self.getNodes()
        destination2vertex2link = {
                                   }
        for dist in nodes:
            distName = dist.getName()
            destination2vertex2link[distName] = {}
            unconnectedNodes = []
            for node in nodes:
                unconnectedNodes.append(node.getName())
            unconnectedNodes.remove(distName)
            
            queue = []
            queue.append(distName)
            while queue:
                nodeName = queue.pop(0)
                edges = [i for i in self.getEdges() if i[0].getName() == nodeName and i[1].getName() in unconnectedNodes]
                for edge in edges:
                    unconnectedNodes.remove(edge[1].getName())
                    queue.append(edge[1].getName())
                    destination2vertex2link[distName][edge[1].getName()] = self.getNode(edge[1].getName()).getBufferTo(self.getNode(nodeName))
                
                edges = [i for i in self.getEdges() if i[1].getName() == nodeName and i[0].getName() in unconnectedNodes]
                for edge in edges:
                    unconnectedNodes.remove(edge[0].getName())
                    queue.append(edge[0].getName())
                    destination2vertex2link[distName][edge[0].getName()] = self.getNode(edge[0].getName()).getBufferTo(self.getNode(nodeName))
        
        for destination in destination2vertex2link.keys():
            for vertex in destination2vertex2link[destination].keys():
                print vertex,"to",destination,"=",destination2vertex2link[destination][vertex]
            print ""
        print destination2vertex2link
        return destination2vertex2link
    
    def constructRoutingTableAllShortestPaths(self):
        print "start shortest path algorithm"
        nodes = self.getNodes()
        destination2vertex2link = {
                                   }
        for dist in nodes:
            distName = dist.getName()
            destination2vertex2link[distName] = {}
            unconnectedNodes = []
            node2distance={}
            for node in nodes:
                unconnectedNodes.append(node.getName())
                node2distance[node.getName()]=10000000
            unconnectedNodes.remove(distName)
            
            queue = []
            queue.append(distName)
            node2distance[distName]=0
            while queue:
                nodeName = queue.pop(0)
                distance=node2distance[nodeName]
                edges = [i for i in self.getEdges() if i[0].getName() == nodeName and i[1].getName() in filter(lambda x : node2distance[x]== distance+1 or  node2distance[x]== 10000000,map(lambda x: x.getName(),nodes))]
                for edge in edges:
                    node2distance[edge[1].getName()]=distance+1
                    if edge[1].getName() in unconnectedNodes:
                        unconnectedNodes.remove(edge[1].getName())
                        queue.append(edge[1].getName())
                    if not edge[1].getName() in destination2vertex2link[distName].keys():
                        destination2vertex2link[distName][edge[1].getName()]=[]
                    destination2vertex2link[distName][edge[1].getName()].append(self.getNode(edge[1].getName()).getBufferTo(self.getNode(nodeName)))
                
                edges = [i for i in self.getEdges() if i[1].getName() == nodeName and i[0].getName() in filter(lambda x : node2distance[x]== distance+1 or  node2distance[x]== 10000000,map(lambda x: x.getName(),nodes))]
                for edge in edges:
                    node2distance[edge[0].getName()]=distance+1
                    if edge[0].getName() in unconnectedNodes:
                        unconnectedNodes.remove(edge[0].getName())
                        queue.append(edge[0].getName())
                    if not edge[0].getName() in destination2vertex2link[distName].keys():
                        destination2vertex2link[distName][edge[0].getName()]=[]
                    destination2vertex2link[distName][edge[0].getName()].append(self.getNode(edge[0].getName()).getBufferTo(self.getNode(nodeName)))
        
        '''
        for destination in destination2vertex2link.keys():
            for vertex in destination2vertex2link[destination].keys():
                print vertex,"to",destination,"=",destination2vertex2link[destination][vertex]
            print ""
        '''
        print "finish shortest path algorithm"
        
        return destination2vertex2link
    
    '''def constructRoutingTableKShortestPaths(self, top_k):
        nodes = self.getNodes()
        graph = "" + str(len(nodes)) + "\n"
        for edge in self.getEdges():
            graph += edge[0].getName() + " " + edge[1].getName() + " 1\n"
            graph += edge[1].getName() + " " + edge[0].getName() + " 1\n"
        kspResults = ksp.ksp(graph, top_k)
        destination2vertex2link = {}
        
        for dist in nodes:
            distName = dist.getName()
            destination2vertex2link[distName] = {}
            for src in nodes:
                srcName = src.getName()
                destination2vertex2link[distName][srcName] = []
                
        for pair in kspResults.split(';')[1:-1]:
            #print "pair:" + pair
            srcName = pair.split('\n')[0].split(',')[0]
            distName = pair.split('\n')[0].split(',')[1]            
            for route in pair.split('\n')[1:-1]:
                throughName = route.split(',')[2].split('->')[1]
                destination2vertex2link[distName][srcName].append(self.getNode(srcName).getBufferTo(self.getNode(throughName)))
            #fix a weird problem of 0,1
            if srcName == '1' and distName == '0':
                for route in pair.split('\n')[1:-1]:
                    throughName = route.split(',')[2].split('->')[-3]
                    destination2vertex2link[srcName][distName].append(self.getNode(distName).getBufferTo(self.getNode(throughName)))
        print destination2vertex2link
        return destination2vertex2link'''

    def getNode(self,name):
        return filter(lambda x: x.getName().__eq__(name),self.nodes)[0]
    
    def getNumberOfEdgesBetweenTwoSwitches(self,s1,s2):
        return len(filter(lambda (v1,v2,a): (v1.__eq__(s1) and v2.__eq__(s2)) or (v1.__eq__(s2) and v2.__eq__(s1)),self.edges))
    
    def constructRoutingScheme(self,destination2vertex2link):
        count = 0
        for destination in destination2vertex2link.keys():
            count += 1
            #print "constructRoutingScheme: " + count + " / "+ len(destination2vertex2link.keys())
            for vertex in destination2vertex2link[destination].keys():
                for edge in destination2vertex2link[destination][vertex]:
                    self.getNode(str(vertex)).addOutgoingLinkForDestination(edge,self.getNode(str(destination)))
        