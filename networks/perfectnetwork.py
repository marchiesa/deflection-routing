import math
import model.switch as switch
import model.network as net

''' NOT YET IMPLEMENTED '''

def unest(l):
    return [i for s in l for i in s]

def Switch(name):
    return switch.Switch(name)

# This class construct a graph that interconnects $n$ vertices in such a way 
# that each vertex has at most degree $3k$ and if in each round are created 
# no more than $k$ packet to a vertex $v$, then no router buffer is required.

class PerfectNetwork(net.Network):
    n = 0
    k = 0
    tree_height = 0

    # construction of a perfect graph
    def __init__(self,n,k):
        '''initialize MultiGraph instance'''
        super(net.Network, self).__init__()
        self.k = k
        if k % n !=0:
            rest = n/k
            self.n = (rest+1)*k
            n = self.n
        rest = n/k
        
        print [[Switch('s_'+str(l)+'_'+str(y)) for y in range(0,k)] for l in range(0,n/k)]
        self.addNodes(unest([[Switch('s_'+str(l)+'_'+str(y)) for y in range(0,k)] for l in range(0,n/k)]))
        self.addNodes(unest([[Switch('i_'+str(l)+'_'+str(y)) for y in range(0,k)] for l in range(0,n/k)]))
        
        self.tree_height = int(math.ceil(math.log(n/k,2)))
        for l in range(0,k):
            for h in range(1,self.tree_height):
                ''' t_l_h_x where l is the l'th tree, h is the level of the tree and x is the x'th node on that level'''
                self.addNodes([Switch('t_'+str(l)+'_'+str(h)+'_'+str(x)) for x in range(0,2**h)])
        
        print self.getNodes()
        print "speakers", self.getSpeakers()
        print "intermediates", self.getIntermediates()
        print "trees", self.getTrees()
        
              
        # add $n/k$ bipartite graphs
        for j in range(0,n/k):
            self.addEdges([(x,y) for x in self.getSpeakers(j) for y in self.getIntermediates(j)])
        
        print self.getEdges()
    
               
        for l in range(0,k):
            intermediates=[row[l] for row in self.getIntermediates()]
            for s in intermediates:
                self.addEdges([(s,Switch('t_'+str(l)+'_'+str(self.tree_height-1)+'_'+str(int(s.name.split('_')[1]) / 2)))]*k)
                
        print self.getEdges()
        
        for l in range(0,k):
            for h in range (2,self.tree_height):
                for s in self.getTreeLevel(l, h):
                    self.addEdges([(s,Switch('t_'+str(l)+'_'+str(h-1)+'_'+str(int(s.name.split('_')[1]) / 2)))]*k)
                
        print self.getEdges()
        
        for l in range(0,k):
            for h in range (2,self.tree_height):
                for s in self.getTreeLevel(l, h):
                    self.addEdges([(s,Switch('t_'+str(l)+'_'+str(h-1)+'_'+str(int(s.name.split('_')[1]) / 2)))]*k)
                
        print self.getEdges()
        

    def getSpeakers(self,partition=None):
        if partition==None:
            return [filter(lambda x : x.name.split('_')[0].__eq__('s') and int(x.name.split('_')[1])==i,self.getNodes()) for i in range(0,self.n/self.k)]
        else:
            return filter(lambda x : x.name.split('_')[0].__eq__('s') and int(x.name.split('_')[1])==partition,self.getNodes())        

    def getIntermediates(self,partition=None):
        if partition==None:
            return [filter(lambda x : x.name.split('_')[0].__eq__('i') and int(x.name.split('_')[1])==i,self.getNodes()) for i in range(0,self.n/self.k)]
        else:
            return filter(lambda x : x.name.split('_')[0].__eq__('i') and int(x.name.split('_')[1])==partition,self.getNodes())        

    def getTrees(self):
        return [[filter(lambda x : x.name.split('_')[0].__eq__('t') and 
                       int(x.name.split('_')[1])==i and 
                       int(x.name.split('_')[2])==h,self.getNodes()) for h in range(1,self.tree_height)]for i in range(0,self.k) ]

    def getTree(self,i):
        return self.getTrees(i)
    
    def getTreeLevel(self,i,level):
        return self.getTrees()[i][level-1]
    
if __name__ == "__main__":
    perfect_network=PerfectNetwork(18,3)
    