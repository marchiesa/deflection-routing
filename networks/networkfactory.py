import networks.networkclique as networkclique
import networks.networkclos as networkclos
import networks.networkclosab as networkclosab
import networks.randomnetwork as randomnetwork
import networks.mocknetwork as mocknetwork
import networks.mocknetwork2 as mocknetwork2
import networks.mocknetwork3 as mocknetwork3
import model.singleton as singleton

''' 
    This class is responsible for constructing  Network objects.
    - self.networkType can be set in order to create a specific network
       available options: clique, clos, clos-ab
'''
class NetworkFactory(singleton.Singleton,object):
    
    networkType = None
    bufferSize = None
    routingPolicyType = None
    
    def __init__(self,networkType='clos',bufferSize=1,routingPolicyType='clos'):
        if self.networkType == None:
            self.networkType=networkType
        if self.bufferSize == None:
            self.bufferSize=bufferSize
        if self.routingPolicyType == None:
            self.routingPolicyType=routingPolicyType
        
    def setNetworkType(self,networkType):
        self.networkType=networkType
    
    def setBufferSize(self,bufferSize):
        self.bufferSize=bufferSize
        
    def setRoutingPolicyType(self,routingPolicyType):
        self.routingPolicyType=routingPolicyType
        
    def getRoutingPolicyType(self):
        return self.routingPolicyType
            
    ''' return a new Network. Use the correct input parameters to configure the 
        specific Network instance. '''
    def getNewNetwork(self,closNumOfLevels=3,closDegree=3,nodes=10,k=3,r=4,p=12):
        if self.networkType.__eq__('clique'):
            n= networkclique.NetworkClique(self.bufferSize)
            return n
        if self.networkType.__eq__('clos'):
            n = networkclos.NetworkClos(closNumOfLevels,closDegree,self.routingPolicyType,self.bufferSize)
            return n
        if self.networkType.__eq__('clos-ab'):
            n = networkclosab.NetworkClosAB(closNumOfLevels,closDegree,self.routingPolicyType,self.bufferSize)
            return n
        if self.networkType.__eq__('random'):
            n = randomnetwork.RandomNetwork(nodes,k,r,p,self.routingPolicyType,self.bufferSize)
            return n
        if self.networkType.__eq__('mock'):
            n = mocknetwork.MockNetwork(self.routingPolicyType,self.bufferSize)
            return n
        if self.networkType.__eq__('mock2'):
            n = mocknetwork2.MockNetwork2(self.routingPolicyType,self.bufferSize)
            return n
        if self.networkType.__eq__('mock3'):
            n = mocknetwork3.MockNetwork3(self.routingPolicyType,self.bufferSize)
            return n
        return None
      

      #TODO
    def importRandomNetworkFromFile(self,fileName):
        randomnetwork.RandomNetwork(fileName)

        