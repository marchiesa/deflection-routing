import sys
import collections

if len(sys.argv) < 2:
	print >> sys.stderr, "usage: %s files port" % (sys.argv[0])
vals = collections.defaultdict(lambda: 0)
total = 0 
with open(sys.argv[1], 'r') as f:
	data = f.readline()
	while data:
		total += 1
		count = 1
		val = int(data.split(';')[count].split(',')[1]);
		min_val = val
		while val == min_val:
			try:
				count += 1
				val = int(data.split(';')[count].split(',')[1])
			except:
				break
		vals[count-1] += 1
		data = f.readline()

so_far = 0
output = ""
for k in sorted(vals.keys()):
	so_far += vals[k]
	output += "(%d %d)\n"%(k, vals[k])

print output