#!/bin/bash
declare -a ktop_list=(12)
declare -a bs_list=(3)

for ktop in ${ktop_list[@]}
do
    cd $ktop
    mkdir output
    echo "cd" $ktop
    rm output/*
    # remove wrong newlines
    for x in `ls *_clos*csv`; do newName=`echo $x |  sed "s#buffer-false-deflection#priority-deflection#"`;  cat $x | awk 'gsub(/\r/,"\n"){printf $0;next}{print}'  > output/100-$newName; done
    cd output
    # parse files
    for i in ${bs_list[@]}; do for x in `ls 100-n*priority-deflection_bs$i.csv`; do cat $x | tail -n +2 | awk '{r=$3;n=(1-r)*100;gsub(/.*/,n,$3);print $0}'; done |  sort -k3n > 200-priority-deflection-reordering-bf$i.csv ; done
    for i in ${bs_list[@]}; do for x in `ls 100-n*upward-deflection_bs$i.csv`; do cat $x | tail -n +2 | awk '{r=$3;n=(1-r)*100;gsub(/.*/,n,$3);print $0}'; done |  sort -k3n > 200-upward-deflection-bf$i.csv ; done
    for i in ${bs_list[@]}; do for x in `ls 100-n*deflection_bs$i*reor*.csv`; do cat $x | tail -n +2 | awk '{r=$3;n=(1-r)*100;gsub(/.*/,n,$3);print $0}'; done |  sort -k3n > 200-priority-deflection-no-reordering-bf$i.csv ; done
    for i in ${bs_list[@]}; do for x in `ls 100-n*clos-deflection_bs$i.csv`; do cat $x | tail -n +2 | awk '{r=$3;n=(1-r)*100;gsub(/.*/,n,$3);print $0}'; done |  sort -k3n > 200-no-priority-deflection-bf$i.csv ; done


    ##########################

    # join results
    for i in ${bs_list[@]}
    do
      join -1 3 -2 3 200-priority-deflection-reordering-bf$i.csv  200-upward-deflection-bf$i.csv  | join - -2 3 200-priority-deflection-no-reordering-bf$i.csv | join - -2 3 200-no-priority-deflection-bf$i.csv | awk 'BEGIN{print "traffic-load priority-deflection upward-deflection priority-deflection-no-reordering no-priority-deflection"} {print $1" " $9" " $24 " " $39 " " $54}' > 300-drop-ratio-comparison-bf$i.csv
      join -1 3 -2 3 200-priority-deflection-reordering-bf$i.csv  200-upward-deflection-bf$i.csv  | join - -2 3 200-priority-deflection-no-reordering-bf$i.csv | join - -2 3 200-no-priority-deflection-bf$i.csv | awk 'BEGIN{print "traffic-load priority-deflection upward-deflection priority-deflection-no-reordering no-priority-deflection"} {print $1" " $12" " $27 " " $42 " " $57}' > 300-delay-comparison-bf$i.csv
      join -1 3 -2 3 200-priority-deflection-reordering-bf$i.csv  200-upward-deflection-bf$i.csv  | join - -2 3 200-priority-deflection-no-reordering-bf$i.csv | join - -2 3 200-no-priority-deflection-bf$i.csv | awk 'BEGIN{print "traffic-load priority-deflection upward-deflection priority-deflection-no-reordering no-priority-deflection"} {print $1" " $13" " $28 " " $43 " " $58}' > 300-delay-unmarked-comparison-bf$i.csv
      join -1 3 -2 3 200-priority-deflection-reordering-bf$i.csv  200-upward-deflection-bf$i.csv  | join - -2 3 200-priority-deflection-no-reordering-bf$i.csv | join - -2 3 200-no-priority-deflection-bf$i.csv | awk 'BEGIN{print "traffic-load priority-deflection upward-deflection priority-deflection-no-reordering no-priority-deflection"} {print $1" " $14" " $29 " " $44 " " $59}' > 300-delay-marked-comparison-bf$i.csv
      join -1 3 -2 3 200-priority-deflection-reordering-bf$i.csv  200-upward-deflection-bf$i.csv  | join - -2 3 200-priority-deflection-no-reordering-bf$i.csv | join - -2 3 200-no-priority-deflection-bf$i.csv | awk 'BEGIN{print "traffic-load priority-deflection upward-deflection priority-deflection-no-reordering no-priority-deflection"} {print $1" " $15" " $30 " " $45 " " $60}' > 300-received-unmarked-comparison-bf$i.csv
      join -1 3 -2 3 200-priority-deflection-reordering-bf$i.csv  200-upward-deflection-bf$i.csv  | join - -2 3 200-priority-deflection-no-reordering-bf$i.csv | join - -2 3 200-no-priority-deflection-bf$i.csv | awk 'BEGIN{print "traffic-load priority-deflection upward-deflection priority-deflection-no-reordering no-priority-deflection"} {print $1" " $16" " $31 " " $46 " " $61}' > 300-received-marked-comparison-bf$i.csv
    done
    
    for file in `ls 100-cdf*.csv`
    do
        mv $file `echo 400-$file | sed "s#100-##" | sed "s#_[^_]*_#_#"`
    done
    
    for file in `ls 400*prio*[0-9].*`  `ls 400*prio*[^g]-u*` `ls 400*prio*[^g]-m*`
    do
        mv $file `echo $file | sed "s#deflection#deflection-reordering#"`
    done
    
    for file in `ls 400*clos-def*`
    do
        mv $file `echo $file | sed "s#deflection#no-priority-deflection#"`
    done
 
    for file in `ls 400*noreor*`
    do
        mv $file `echo $file | sed "s#-noreordering##" | sed "s#deflection#deflection-no-reordering#"`
    done
    cd ..
    cd ..
    echo "cd .." 
done


ktop_list_para=""
bs_list_para=""
for ktop in ${ktop_list[@]}
do
  ktop_list_para=$ktop_list_para","$ktop
done
for i in ${bs_list[@]}
do
  bs_list_para=$bs_list_para","$i
done

python generate_plot_script_clos_deflection_vs_routing.py $ktop_list_para $bs_list_para > commands.txt

gnuplot < commands.txt



