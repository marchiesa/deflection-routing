import networks.networkfactory as networkfactory
import model.trafficgenerator as tg
import model.trafficgeneratormoretraffic as tgmt
import model.trafficgeneratorFlowCDF as tgmtcdf
import model.statistics as stat
import model.singleton as singleton
import model.packet as packet
import model.switchfactory as switchfactory
from collections import deque
import os


class Simulator(singleton.Singleton,object):
    
    maxThroughtput=0
    maxBufferCapacity=0
    
    ''' max number of packets to a specific vertex generated in a single round '''
    def setMaxNumberPacketsPerNode(self,maxNumberPacketsPerNode):
        self.maxNumberPacketsPerNode=maxNumberPacketsPerNode
        
    def setNetwork(self,network):
        self.network = network
        
    def getNetwork(self):
        return self.network
    
    ''' populates nodes 'ingress' buffers with randomly generated packets.
        Refer to model.trafficgenerator for details about parameters for
        traffic generation '''
    def originateTraffic(self,limitInputRate=0):
        self.traffic.generateRandomTraffic(True,self.iterations-stat.Statistics().getIteration() )
        
            
    ''' move packets among switch buffers according to the routing policy of 
        each switch '''
    def nextStep(self,reordering=True):
        nodes = self.getNetwork().getNodes()
        outputProcess = {}
        
        
        ''' get first output from each port of a switch '''
        packets={}
        for node in nodes:
            packets[node]={}
            for (a,n) in node.getPorts():
                if len(n.getPorts()[(a,node)]) > 0:
                    packets[node][(a,n)]=n.getPorts()[(a,node)].popleft()
                    packets[node][(a,n)].ttl -= 1
                    #print "**hops",packets[node][(a,n)].ttl 
                    packets[node][(a,n)].lastHop = n;
        
        
        
        ''' compute switch routing decisions '''
        for node in nodes:
            outputProcess[node]=node.processIngoingPackets(packets[node])
            
        ##print "##########output",outputProcess[self.getNetwork().getNode(1,0)]
            
        ''' move packets among buffers according to 'outputProcess' '''                       
        for node in outputProcess.keys():
            ##print "node",node, "buffer",outputProcess[node].keys(), outputProcess[node].values()
            for (interfaceName,neighbor) in outputProcess[node].keys():
                
                output = outputProcess[node][interfaceName,neighbor]
                ##print "i'm", node, " sending output:", output.__repr__(), "to", neighbor.__repr__(), "through", interfaceName 
                
                buffer = node.getBufferByInterfaceAndNode(interfaceName,neighbor)
                ##print "buffer",buffer
                if reordering and networkfactory.NetworkFactory().bufferSize>1:
                    
                    deflectedPackets =deque(filter(lambda x: x.isDeflected(),output))
                    nonDeflectedPackets =filter(lambda x: not x.isDeflected(),output)
                    bufferDeflectedPackets =filter(lambda x:  x.isDeflected(),buffer)
                    bufferNonDeflectedPackets =filter(lambda x: not  x.isDeflected(),buffer)
                    #if len(deflectedPackets) > 0 or len(bufferDeflectedPackets) >0:
                    #    #print "buffer", buffer
                    #    #print "output", output
                    buffer=deque([])
                    buffer.extend(bufferNonDeflectedPackets)
                    buffer.extend(nonDeflectedPackets)
                    buffer.extend(bufferDeflectedPackets)
                    buffer.extend(deflectedPackets)
                    #if len(deflectedPackets) > 0 or len(bufferDeflectedPackets) >0:
                    ##print "final buffer", buffer
                    node.getPorts()[(interfaceName,neighbor)]=buffer
                else:
                    buffer.extend(output)
        stat.Statistics().recordBuffers(self.getNetwork())    
                
    @staticmethod
    def generateExpNetwork(levels=3,networkType='clos',routingPolicy='deflection',bufferSize=1,randomN=0,randomR=0,reordering=True,nBestPorts=12):
        k=3
        l = levels   #number of levels 
        if not networkType.__eq__("random"):
            netFactory = networkfactory.NetworkFactory()
            netFactory.setNetworkType(networkType)
            netFactory.setBufferSize(bufferSize)
            netFactory.setRoutingPolicyType(routingPolicy)
            network = netFactory.getNewNetwork(closNumOfLevels=l, closDegree=k)
        else :
            netFactory = networkfactory.NetworkFactory()
            netFactory.setNetworkType(networkType)
            netFactory.setBufferSize(bufferSize)
            netFactory.setRoutingPolicyType(routingPolicy)
            network = netFactory.getNewNetwork(n=randomN,r=randomR,p=nBestPorts)
        return network

    ''' general method to run several experiments and collect some data in 
        CVS format '''
    @staticmethod
    def collectData(limitRate,levels,network, networkType='clos',routingPolicy='deflection',bufferSize=1,randomN=0,randomR=0,reordering=True,nBestPorts=12):
        import csv

        directory = "data/"+str(nBestPorts)
        if not os.path.exists(directory):
            os.makedirs(directory)
    
        k=22
        l = levels   #number of levels 
        if not networkType.__eq__("random"):
            fileName=directory+"/n"+str(l)+'levels_lr'+str(limitRate)+'_'+networkType+'_'+routingPolicy+'_bs'+str(bufferSize)
            if not reordering:
                fileName+="-noreordering"
            fileName+=".csv"

        else :
            fileName= directory+"/n"+str(randomN)+'_r'+str(randomR)+'_lr'+str(limitRate)+'_'+networkType+'_'+routingPolicy+'_bs'+str(bufferSize)
            if not  reordering:
                fileName+="-noreordering"
            fileName+=".csv"

            
        with open(fileName, 'wb') as csvfile:
            writer = csv.writer(csvfile, delimiter=' ',
                quotechar='|', quoting=csv.QUOTE_MINIMAL)
            ''' CVS format legend '''
            if not networkType.__eq__("random"):        
                writer.writerow(['levels','vertex_degree','limit_rate','drop','success','drop_ratio','#iteration','buffer_size','averageDrop','averageReceived','averageSent','averageDelay','averageUnmarkedDelay','averageMarkedDelay','averageUnmarkedReceived','averageMarkedReceived','averageLinkUtilization'])
            else:
                writer.writerow(['randomN','randomR','limit_rate','drop','success','drop_ratio','#iteration','buffer_size','averageDrop','averageReceived','averageSent','averageDelay','averageUnmarkedDelay','averageMarkedDelay','averageUnmarkedReceived','averageMarkedReceived','averageLinkUtilization'])
            stat.Statistics().countDrop=0
            stat.Statistics().countSuccess=0
            stat.Statistics().numberOfPacketsSent=0 
                
                    
            print "nodes", network.getSpeakers()
            sim = Simulator()
            sim.setNetwork(network)
            sim.setMaxNumberPacketsPerNode(k)
            Simulator.maxBufferCapacity=0
            for node in network.getNodes():
                Simulator.maxBufferCapacity+= len(node.getPorts())
            Simulator.maxThroughtput=0
            print "#nodes", len(network.getSpeakers())
            for node in network.getSpeakers():
                Simulator.maxThroughtput+= len(node.getPorts())
                
            print Simulator.maxThroughtput
            print limitRate
            sim.iterations = max(5000000/Simulator.maxThroughtput/(1-limitRate),10000)
            
            i=sim.iterations  
            
            fileName="data/sample_workload_a6_t"+str(int(limitRate*10))+".txt"
            print fileName
            sim.traffic = tgmtcdf.TrafficGeneratorFlowCDF(sim.network.getSpeakers(),sim.maxNumberPacketsPerNode,fileName=fileName)
            #sim.traffic = tgmt.TrafficGeneratorMoreTraffic(sim.network.getSpeakers(),sim.maxNumberPacketsPerNode)
            sim.traffic.limitInputRate = limitRate
            
            print "originating traffic"
            ese = endSimulationEstimator()
            #while not ese.isSteady() and i > 0:
            import time
            s = time.time()
            while sim.iterations-i < 5000:
                #sim.traffic.getMaxTime():
                ##print "originating traffic",k
                stat.Statistics().cleanForNewIteration()
                stat.Statistics().setIteration(i)
                sim.originateTraffic(limitRate)
                ##print "next step -1",
                sim.nextStep(reordering)
                if (sim.iterations -i) % (int(sim.iterations/10000)+1) == 0:
                    print "iteration", sim.iterations - i, "out of", sim.traffic.getMaxTime()
                    pass
                #print "nodes", network.getSpeakers()
                #print "throughtput", Simulator.maxThroughtput
                numberOfDroppedPackets = stat.Statistics().getLastIterationDrop()
                numberOfGeneratedPackets = stat.Statistics().getLastIterationNumberOfPacketsSent()
                numberOfReceivedPackets = stat.Statistics().getLastIterationSuccess()
                delay = stat.Statistics().getLastIterationAverageDelay()
                markedDelay = stat.Statistics().getLastIterationAverageMarkedDelay()
                unmarkedDelay = stat.Statistics().getLastIterationAverageUnmarkedDelay() 
                markedPackets=   stat.Statistics().getLastIterationMarkedSuccess()
                unmarkedPackets=   stat.Statistics().getLastIterationUnmarkedSuccess()
                delay2numberOfPackets = stat.Statistics().getLastIterationDelays()
                delay2numberOfUnmarkedPackets = stat.Statistics().getLastIterationDelaysUnmarked()
                delay2numberOfMarkedPackets = stat.Statistics().getLastIterationDelaysMarked()
                hops2numberOfPackets = stat.Statistics().getLastIterationHops()
                hops2numberOfPacketsUnmarked = stat.Statistics().getLastIterationHopsUnmarked()
                hops2numberOfPacketsMarked = stat.Statistics().getLastIterationHopsMarked()
                stat.Statistics().recordBuffers(network)
                
                
                ese.updateEstimator([numberOfDroppedPackets,numberOfGeneratedPackets,numberOfReceivedPackets,delay,unmarkedDelay,markedDelay,unmarkedPackets,markedPackets,delay2numberOfPackets,delay2numberOfUnmarkedPackets,delay2numberOfMarkedPackets,stat.Statistics().getAverageLinkUtilization(),hops2numberOfPackets,hops2numberOfPacketsUnmarked,hops2numberOfPacketsMarked]) 
                i-=1 
            if i <= 0:
                #not converge
                if not os.path.exists("data/log_random"):
                    os.makedirs("data/log_random")
                logFileName= "data/log_random"+"/" + "p="+str(nBestPorts)+"n"+str(randomN)+'_r'+str(randomR)+'_lr'+str(limitRate)+'_'+networkType+'_'+routingPolicy+'_bs'+str(bufferSize)
                logFile = open(logFileName,'w')
                logFile.write("not converge")
                logFile.close()
                return
                #print "i:",iterations-i
                
                '''fileBuffers = open("data/space/space"+str(iterations-i)+"_bs"+str(bufferSize), 'w')
                row="_ "
                for j in range(0,bufferSize+1):
                    if j==0:
                        row+="empty "
                    elif j==bufferSize:
                        row+="full "
                    else:
                        row+=str(j)+" "
                row+="dropped received "    
                
                fileBuffers.write(row+"\n")
                row="buffer-load-bs="+str(bufferSize)+" "
                for j in range(0,bufferSize+3):
                    if j >=bufferSize+1:
                        row+="0 "
                    else:
                        row+=str(stat.Statistics().getSpace2NumberOfSwitches()[j]/float(Simulator.maxBufferCapacity))+" "
                fileBuffers.write(row+"\n")
                #print Simulator.maxBufferCapacity, stat.Statistics().getSpace2NumberOfSwitches()[0]
                
                
                row="drop-rate-bs="+str(bufferSize)+" "
                for j in range(0,bufferSize+1):
                    row+="0 "
                row+=str(stat.Statistics().getLastIterationDrop()/float(stat.Statistics().getLastIterationNumberOfPacketsSent()))+" "
                row+=str(1-(stat.Statistics().getLastIterationDrop()/float(stat.Statistics().getLastIterationNumberOfPacketsSent())))+" "                
                    
                fileBuffers.write(row+"\n")
                fileBuffers.close()'''   
                     
            print "time",time.time()-s
            ese.computeResults()
            averageDrop=ese.getResults()['averageDrop']
            averageReceived=ese.getResults()['averageReceived']
            averageSent=ese.getResults()['averageSent']
            averageDelay=ese.getResults()['averageDelay']
            averageUnmarkedDelay=ese.getResults()['averageUnmarkedDelay']
            averageMarkedDelay=ese.getResults()['averageMarkedDelay']
            averageUnmarkedReceived=ese.getResults()['averageUnmarkedReceived']
            averageMarkedReceived=ese.getResults()['averageMarkedReceived']
            cdfDelayMap=ese.getResults()['cdfDelays']
            cdfDelayMapUnmarked=ese.getResults()['cdfDelaysUnmarked']
            cdfDelayMapMarked=ese.getResults()['cdfDelaysMarked']
            averageLinkUtilization=ese.getResults()['averageLinkUtilization']
            cdfHopsMap=ese.getResults()['cdfHops']
            cdfHopsMapMarked=ese.getResults()['cdfHopsMarked']
            cdfHopsMapUnmarked=ese.getResults()['cdfHopsUnmarked']
            #print averageDrop/float(averageSent)
            #print averageReceived/float(averageSent)
            #print averageSent
            #print averageDelay
            #print averageUnmarkedDelay
            #print averageMarkedDelay
            #print averageUnmarkedReceived/float(averageReceived)
            
            if not networkType.__eq__("random"):        
                writer.writerow([l,k,limitRate,stat.Statistics().getDrop(),stat.Statistics().getSuccess(),stat.Statistics().getDropRatio()*100,sim.iterations-i,bufferSize,(averageDrop/float(averageSent))*100,(averageReceived/float(averageSent))*100,averageSent,averageDelay,averageUnmarkedDelay,averageMarkedDelay,(averageUnmarkedReceived/float(averageReceived))*100,(averageMarkedReceived/float(averageReceived))*100,averageLinkUtilization])
            else:
                writer.writerow([randomN,randomR,limitRate,stat.Statistics().getDrop(),stat.Statistics().getSuccess(),stat.Statistics().getDropRatio()*100,sim.iterations-i,bufferSize,(averageDrop/float(averageSent))*100,(averageReceived/float(averageSent))*100,averageSent,averageDelay,averageUnmarkedDelay,averageMarkedDelay,(averageUnmarkedReceived/float(averageReceived))*100,(averageMarkedReceived/float(averageReceived))*100,averageLinkUtilization])
            
            #write CDF files
            splittedFileName = fileName.split("/")
            fileBufferName=""    
            for i in range(0,len(splittedFileName)-1):
                fileBufferName+=splittedFileName[i]+"/"
            fileBufferName+='cdf_'+splittedFileName[len(splittedFileName)-1]
            fileCDF = open(fileBufferName, 'w')
            row="#delay fraction-of-delayed-packets"
            fileCDF.write(row+"\n")
            for delay in cdfDelayMap.keys():
                row=str(delay)+" "+str(cdfDelayMap[delay])
                fileCDF.write(row+"\n")
            fileCDF.close()
            
            #write CDF files
            splittedFileName = fileName.split("/")
            fileBufferName=""    
            for i in range(0,len(splittedFileName)-1):
                fileBufferName+=splittedFileName[i]+"/"
            fileBufferName+='cdf_'+splittedFileName[len(splittedFileName)-1]
            fileBufferName=fileBufferName.replace('.csv','-unmarked.csv')
            fileCDF = open(fileBufferName, 'w')
            row="#delay fraction-of-delayed-packets"
            fileCDF.write(row+"\n")
            for delay in sorted(cdfDelayMapUnmarked.keys()):
                row=str(delay)+" "+str(cdfDelayMapUnmarked[delay])
                fileCDF.write(row+"\n")
            fileCDF.close()
                
            #write CDF files
            splittedFileName = fileName.split("/")
            fileBufferName=""    
            for i in range(0,len(splittedFileName)-1):
                fileBufferName+=splittedFileName[i]+"/"
            fileBufferName+='cdf_'+splittedFileName[len(splittedFileName)-1]
            fileBufferName=fileBufferName.replace('.csv','-marked.csv')
            fileCDF = open(fileBufferName, 'w')
            row="#delay fraction-of-delayed-packets" 
            fileCDF.write(row+"\n")
            for delay in sorted(cdfDelayMapMarked.keys()):
                row=str(delay)+" "+str(cdfDelayMapMarked[delay])
                fileCDF.write(row+"\n")
            fileCDF.close()
            
            #write CDF files
            splittedFileName = fileName.split("/")
            fileBufferName=""    
            for i in range(0,len(splittedFileName)-1):
                fileBufferName+=splittedFileName[i]+"/"
            fileBufferName+='hops_'+splittedFileName[len(splittedFileName)-1]
            fileCDF = open(fileBufferName, 'w')
            row="#hops action-of-delayed-packets"
            fileCDF.write(row+"\n")
            for hops in sorted(cdfHopsMap.keys()):
                row=str(hops)+" "+str(cdfHopsMap[hops])
                fileCDF.write(row+"\n")
            fileCDF.close()
            
            #write CDF files
            splittedFileName = fileName.split("/")
            fileBufferName=""    
            for i in range(0,len(splittedFileName)-1):
                fileBufferName+=splittedFileName[i]+"/"
            fileBufferName+='hops_'+splittedFileName[len(splittedFileName)-1]
            fileBufferName=fileBufferName.replace('.csv','-marked.csv')
            fileCDF = open(fileBufferName, 'w')
            row="#hops action-of-delayed-packets"
            fileCDF.write(row+"\n")
            for hops in sorted(cdfHopsMapMarked.keys()):
                row=str(hops)+" "+str(cdfHopsMapMarked[hops])
                fileCDF.write(row+"\n")
            fileCDF.close()
            
            #write CDF files
            splittedFileName = fileName.split("/")
            fileBufferName=""    
            for i in range(0,len(splittedFileName)-1):
                fileBufferName+=splittedFileName[i]+"/"
            fileBufferName+='hops_'+splittedFileName[len(splittedFileName)-1]
            fileBufferName=fileBufferName.replace('.csv','-unmarked.csv')
            fileCDF = open(fileBufferName, 'w')
            row="#hops action-of-delayed-packets"
            fileCDF.write(row+"\n")
            for hops in sorted(cdfHopsMapUnmarked.keys()):
                row=str(hops)+" "+str(cdfHopsMapUnmarked[hops])
                fileCDF.write(row+"\n")
            fileCDF.close()
            
            sim.traffic = None
             
            
        
        
class endSimulationEstimator:
    
    def __init__(self):
        #self.values=deque([])
        self.numberOfIntervals = 4
        self.sizeOfEachInterval = max(10,25000/Simulator.maxThroughtput)
        print "size",self.sizeOfEachInterval
        self.values=[]
        self.firstEmptyInterval=0
        self.hasRestart = False
        self.results = {}
        
        for _ in range(0,self.numberOfIntervals):
            self.values.append(deque([]))
        
    
    
    def restartEstimation(self):
        #print "-----------------------restart"
        self.firstEmptyInterval=0
        self.values=[]
        for _ in range(0,self.numberOfIntervals):
            self.values.append(deque([]))
        
        
    def updateEstimator(self,values):
        
        #populate until it has enough values
        if self.firstEmptyInterval <self.numberOfIntervals:
            self.values[self.firstEmptyInterval].append([values[0],values[1],values[2], values[3],values[4],values[5],values[6],values[7],values[8],values[9],values[10],values[11],values[12],values[13],values[14]])
            #print "size",len(self.values[self.firstEmptyInterval])
            if len (self.values[self.firstEmptyInterval]) ==self.sizeOfEachInterval:
                self.firstEmptyInterval+=1
        else:    
            #update structure 
            for i in range(0,len(self.values)):
                value = self.values[i].popleft()
                if i!=0:
                    self.values[i-1].append(value)                
            self.values[self.numberOfIntervals-1].append([values[0],values[1],values[2], values[3],values[4],values[5],values[6],values[7],values[8],values[9],values[10],values[11],values[12],values[13],values[14]])
            #print " drop", values[0]
            #print " sent",values[1]
            #print " received",values[2]
            #print " delay",values[3]
            #print " Udelay",values[4]
            #print " Mdelay",values[5]
         
                
    def isSteady(self):
        if self.firstEmptyInterval <self.numberOfIntervals:
            return False
        else:
            averagesDrop = map(lambda x : self.computeAverageTwoValues(x,0,1),self.values)
            #print " averagesDrop",averagesDrop
            averagesReceived = map(lambda x : self.computeAverageTwoValues(x,2,1),self.values)
            #print " averagesReceived",averagesReceived
            overallAverageDrop = self.computeAverageOneValue(averagesDrop)
            overallAverageReceived = self.computeAverageOneValue(averagesReceived)
            #print " overallDrop",overallAverageDrop
            #print " overallReceived",overallAverageReceived
            print " sum: ",1 - (overallAverageDrop + overallAverageReceived) 
            
            if overallAverageDrop + overallAverageReceived > 0.995:
                if self.hasRestart == False:
                    #totalPacketsSent = sum(map(lambda x : self.computeSum(x,1),self.values))
                    #self.sizeOfEachInterval = min(self.sizeOfEachInterval, round(self.sizeOfEachInterval*100000/totalPacketsSent))
                    #print self.sizeOfEachInterval
                    self.restartEstimation()
                    self.hasRestart = True
                    return False
                else:
                    #delay also satisfy, return True:
                    averageDrop = self.computeAverage(map(lambda x : self.computeAverage(x,0),self.values),0)[0]
                    averageSent = self.computeAverage(map(lambda x : self.computeAverage(x,1),self.values),0)[0]
                    averageReceived = self.computeAverage(map(lambda x : self.computeAverage(x,2),self.values),0)[0]
                    averageDelay = self.computeAverage(map(lambda x : self.computeAverage(x,3),self.values),0)[0]
                    averageUnmarkedDelay = self.computeAverage(map(lambda x : self.computeAverage(x,4),self.values),0)[0]
                    averageMarkedDelay = self.computeAverage(map(lambda x : self.computeAverage(x,5),self.values),0)[0]
                    averageUnmarkedReceived = self.computeAverage(map(lambda x : self.computeAverage(x,6),self.values),0)[0]
                    averageMarkedReceived = self.computeAverage(map(lambda x : self.computeAverage(x,7),self.values),0)[0]
                    cdfDelays = self.computeCDFDelay(self.values,8,0,2) # 8,delay-map 0,drop 1,received
                    cdfDelaysUnmarked = self.computeCDFDelay(self.values,9,0,2) # 8,delay-map 0,drop 1,received
                    cdfDelaysMarked = self.computeCDFDelay(self.values,10,0,2) # 8,delay-map 0,drop 1,received
                    averageLinkUtilization = self.computeAverage(map(lambda x : self.computeAverage(x,11),self.values),0)[0]
                    cdfHops = self.computeCDFDelayNoDrop(self.values,12,0,2) # 8,delay-map 0,drop 1,received
                    cdfHopsMarked = self.computeCDFDelayNoDrop(self.values,13,0,2) # 8,delay-map 0,drop 1,received
                    cdfHopsUnmarked = self.computeCDFDelayNoDrop(self.values,14,0,2) # 8,delay-map 0,drop 1,received
                    
                    if averageUnmarkedDelay == None:
                        averageUnmarkedDelay = 0
                    if averageMarkedDelay == None:
                        averageMarkedDelay = 0
                    
                    self.results['averageDrop']=averageDrop
                    self.results['averageReceived']=averageReceived
                    self.results['averageSent']=averageSent
                    self.results['averageDelay']=averageDelay
                    self.results['averageUnmarkedDelay']=averageUnmarkedDelay
                    self.results['averageMarkedDelay']=averageMarkedDelay
                    self.results['averageUnmarkedReceived']=averageUnmarkedReceived
                    self.results['averageMarkedReceived']=averageMarkedReceived
                    self.results['cdfDelays']=cdfDelays
                    self.results['cdfDelaysUnmarked']=cdfDelaysUnmarked
                    self.results['cdfDelaysMarked']=cdfDelaysMarked
                    self.results['averageLinkUtilization']=averageLinkUtilization
                    self.results['cdfHops']=cdfHops
                    self.results['cdfHopsUnmarked']=cdfHopsUnmarked    
                    self.results['cdfHopsMarked']=cdfHopsMarked
                    
                    #results.append()
                    return True
                '''overallAverageDrop = self.computeAverageOneValue(averagesDrop)
                overallAverageReceived = self.computeAverageOneValue(averagesReceived)
                #print "overallDrop",overallAverageDrop
                #print "overallReceived",overallAverageReceived
                maxDifferenceDrop = self.getMaxDifference(averagesDrop)
                maxDifferenceReceived = self.getMaxDifference(averagesReceived)
                #print "maxDrop", maxDifferenceDrop
                #print "maxReceived", maxDifferenceReceivedx
                if overallAverageDrop == 0:
                    #check whether the throughput is stable
                else: 
                    #check whether the drop rate is stable
                    #print "ratio",maxDifference/overallAverage'''

            return False
        
    def getResults(self):
        return self.results    
         
    def computeResults(self):
        averageDrop = self.computeAverage(map(lambda x : self.computeAverage(x,0),self.values),0)[0]
        averageSent = self.computeAverage(map(lambda x : self.computeAverage(x,1),self.values),0)[0]
        averageReceived = self.computeAverage(map(lambda x : self.computeAverage(x,2),self.values),0)[0]
        averageDelay = self.computeAverage(map(lambda x : self.computeAverage(x,3),self.values),0)[0]
        averageUnmarkedDelay = self.computeAverage(map(lambda x : self.computeAverage(x,4),self.values),0)[0]
        averageMarkedDelay = self.computeAverage(map(lambda x : self.computeAverage(x,5),self.values),0)[0]
        averageUnmarkedReceived = self.computeAverage(map(lambda x : self.computeAverage(x,6),self.values),0)[0]
        averageMarkedReceived = self.computeAverage(map(lambda x : self.computeAverage(x,7),self.values),0)[0]
        cdfDelays = self.computeCDFDelay(self.values,8,0,2) # 8,delay-map 0,drop 1,received
        cdfDelaysUnmarked = self.computeCDFDelay(self.values,9,0,6) # 8,delay-map 0,drop 1,received
        cdfDelaysMarked = self.computeCDFDelay(self.values,10,0,7) # 8,delay-map 0,drop 1,received
        averageLinkUtilization = self.computeAverage(map(lambda x : self.computeAverage(x,11),self.values),0)[0]
        cdfHops = self.computeCDFDelayNoDrop(self.values,12,0,2) # 8,delay-map 0,drop 1,received
        cdfHopsMarked = self.computeCDFDelayNoDrop(self.values,14,0,7) # 8,delay-map 0,drop 1,received
        cdfHopsUnmarked = self.computeCDFDelayNoDrop(self.values,13,0,6) # 8,delay-map 0,drop 1,received
        
        if averageUnmarkedDelay == None:
            averageUnmarkedDelay = 0
        if averageMarkedDelay == None:
            averageMarkedDelay = 0
        
        self.results['averageDrop']=averageDrop
        self.results['averageReceived']=averageReceived
        self.results['averageSent']=averageSent
        self.results['averageDelay']=averageDelay
        self.results['averageUnmarkedDelay']=averageUnmarkedDelay
        self.results['averageMarkedDelay']=averageMarkedDelay
        self.results['averageUnmarkedReceived']=averageUnmarkedReceived
        self.results['averageMarkedReceived']=averageMarkedReceived
        self.results['cdfDelays']=cdfDelays
        self.results['cdfDelaysUnmarked']=cdfDelaysUnmarked
        self.results['cdfDelaysMarked']=cdfDelaysMarked
        self.results['averageLinkUtilization']=averageLinkUtilization
        self.results['cdfHops']=cdfHops
        self.results['cdfHopsUnmarked']=cdfHopsUnmarked    
        self.results['cdfHopsMarked']=cdfHopsMarked
        
    def computeAverageTwoValues(self,values,index1,index2):
        sum1=0
        sum2=0
        for subvalues in values:
            sum1+=subvalues[index1]
            sum2+=subvalues[index2]
        if sum2 ==0:
            return 0 
        return sum1/float(sum2)
    
    def computeAverageOneValue(self,values):
        sum1=0
        for v1 in values:
            sum1+=v1
        return (sum1/float(len(values)))

    def computeSum(self,values,index1):
        sum1=0
        for subvalues in values:
            sum1+=subvalues[index1]
        return sum1
    
    def computeAverage(self,values,index1):
        sum1=0
        count = 0
        for subvalues in values:
            if subvalues[index1] != None:
                count+=1
                sum1+=subvalues[index1]
        if count== 0:
            return [None]
        return [sum1/float(count)]

    def computeCDFDelay(self, values,indexDelayMap,indexDropValue,indexReceivedValue):
        #TODO
        pass
        #unnest values
        unnestedValues = [ y  for x in self.values for y in x]
        overallDelayMap = {}
        overallnumberOfDroppedPackets = 0
        overallnumberOfReceivedPackets = 0
        
        #debug
        overallNuberOfPacketsInDelayMap = 0
        for value in unnestedValues:
            delayMap = value[indexDelayMap]
            numberOfDroppedPackets = value[indexDropValue]
            numberOfReceivedPackets = value[indexReceivedValue]
            
            #update overall values
            for delay in delayMap.keys():
                if not delay in overallDelayMap.keys():
                    overallDelayMap[delay] = 0
                overallDelayMap[delay]+=delayMap[delay]
                overallNuberOfPacketsInDelayMap+=delayMap[delay]
                
            overallnumberOfDroppedPackets+=numberOfDroppedPackets
            overallnumberOfReceivedPackets+=numberOfReceivedPackets
        
        overallNumberOfPackets = overallnumberOfDroppedPackets + overallnumberOfReceivedPackets
        
        sumOfFractions=0
        for delay in sorted(overallDelayMap.keys()):
            fraction = overallDelayMap[delay]/float(overallNumberOfPackets)*100
            sumOfFractions+=fraction
            overallDelayMap[delay]=sumOfFractions
            
        return overallDelayMap
 
    def computeCDFDelayNoDrop(self, values,indexDelayMap,indexDropValue,indexReceivedValue):
        #TODO
        pass
        #unnest values
        unnestedValues = [ y  for x in self.values for y in x]
        #print "----"
        overallDelayMap = {}
        overallnumberOfReceivedPackets = 0
        
        #debug
        overallNuberOfPacketsInDelayMap = 0
        for value in unnestedValues:
            delayMap = value[indexDelayMap]
            numberOfReceivedPackets = value[indexReceivedValue]
            #print "djsaio", numberOfReceivedPackets
            #print "value", value
            
            #update overall values
            for delay in delayMap.keys():
                if not delay in overallDelayMap.keys():
                    overallDelayMap[delay] = 0
                overallDelayMap[delay]+=delayMap[delay]
                overallNuberOfPacketsInDelayMap+=delayMap[delay]
                
            overallnumberOfReceivedPackets+=numberOfReceivedPackets
        
        overallNumberOfPackets =  overallnumberOfReceivedPackets
        
        sumOfFractions=0
        for delay in sorted(overallDelayMap.keys()):
            fraction = overallDelayMap[delay]/float(overallNumberOfPackets)*100
            sumOfFractions+=fraction
            overallDelayMap[delay]=sumOfFractions
            
        return overallDelayMap
    
         
if __name__ == "__main__":
    
    #Simulator.testClosBackward()
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-r", "--limit-rate", dest="limitRate", help="limits number of packets sent at each round")
    parser.add_option("-l", "--levels", dest="levels", help="number of levels")
    parser.add_option("-n", "--network", dest="networkType", help="clique, clos, closab")
    parser.add_option("-s", "--strategy", dest="routingPolicy", help="clos-deflection,clos-no-deflection,clos-upward-deflection,clos-smart-deflection")
    parser.add_option("-b", "--buffer-size", dest="bufferSize", help="size of buffer")
    parser.add_option("-a",  dest="randomN", help="number of vertices")
    parser.add_option("-q",  dest="randomR", help="degree of vertices")
    parser.add_option("--no-reordering",  dest="reordering", help="reordering")
    parser.add_option("-p",  dest="nBestPorts", help="nBestPorts")
    (options, args) = parser.parse_args()

    limitRate = None
    if options.limitRate != None:
        limitRate=float(options.limitRate)
    levels = None
    if options.levels != None:
        levels=int(options.levels)
    networkType = None
    if options.networkType != None:
        networkType=options.networkType
    routingPolicy = None
    if options.routingPolicy != None:
        routingPolicy=options.routingPolicy
    bufferSize = None
    if options.bufferSize != None:
        bufferSize=int(options.bufferSize)
    randomN = 10
    if options.randomN != None:
        randomN=int(options.randomN)
    randomR = 4
    if options.randomR != None:
        randomR=int(options.randomR)
    reordering = True
    if options.reordering != None:
        reordering = False
    if options.nBestPorts == None:
        options.nBestPorts = "1"
    nBestPorts = int(options.nBestPorts)
        
            
    #limitRates=map(lambda x : 0.1*x,range(0,8))
    #for limitRate in limitRates:
    print limitRate,levels,networkType,routingPolicy,bufferSize,randomN,randomR
    network = Simulator.generateExpNetwork(levels,networkType,routingPolicy,bufferSize,randomN,randomR,reordering=reordering,nBestPorts=nBestPorts)
    
    #for limitRate in map(lambda x:x/10.0, range(9,-1,-1)):
    for limitRate in [0.5]:
        network.clearBuffers()
        stat.Statistics().resetStatistics()
        Simulator.collectData(limitRate,levels,network,networkType,routingPolicy,bufferSize,randomN,randomR,reordering=reordering,nBestPorts=nBestPorts)
        
    
