#!/bin/bash

#for s in clos-no-deflection clos-deflection clos-upward-deflection clos-smart-deflection clos-buffer-false-deflection random-deflection
for s in random-deflection random-routing
do
    for b in 1 2 3 4 5 6 7 8 9 10
    do
        echo $s " " $b
        for nPort in 1 2 3 4 5 6 7 8 9 10 11 12
        do
            python simulator.py -r 0 -n random -s $s -b $b -a 432 -q 16 -p $nPort > log/random_$s_$b_$nPort.txt&
        done
    done
done 


