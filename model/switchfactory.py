
import model.switchclique as switchclique
import model.switchclos as switchclos
import model.switchrandom as switchrandom   
import model.singleton as singleton
import model.routingpolicyclosshortcut as rpcs
import model.routingpolicyclosshortcutoutputbuffer as rpcsob
import model.routingpolicyclosshortpriority as rpcsp
import model.routingpolicyclosshortprioritybuffer as rpcspb
import model.routingpolicyrandomrandom as rprr
import model.routingpolicyrandomrandomavoidloop as rprral

''' 
    This class is responsible for constructing  Switch objects.
    - self.networkType can be set in order to create switch of a certain type
       available options: clique, clos 
'''
class SwitchFactory(singleton.Singleton,object):
    
    networkType=None
    bufferSize=None
    
    def __init__(self,switchType='clique',bufferSize=1):
        if self.networkType == None:
            self.networkType=switchType
        if self.bufferSize == None:
            self.bufferSize=bufferSize
        
    ''' return a new SwitchClos. Use the correct input parameters to configure the 
        Switch instance. '''
    def getNewSwitch(self,name=None,bufferSize=None,level=None,position=None):
        if not self.bufferSize == None and  bufferSize==None:
            bufferSize = self.bufferSize
        if self.networkType.__eq__('clos-deflection'):
            s = switchclos.SwitchClos(level,position,bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,True,True))
            return s
        if self.networkType.__eq__('clos-no-deflection'):
            s = switchclos.SwitchClos(level,position,bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            return s
        if self.networkType.__eq__('clos-upward-deflection'):
            s = switchclos.SwitchClos(level,position,bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,True,False))
            return s
        if self.networkType.__eq__('clos-smart-deflection'):
            s = switchclos.SwitchClos(level,position,bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rpcsp.RoutingPolicyClosShortcutPriority(s))
            return s
        if self.networkType.__eq__('clos-buffer-true-deflection'):
            s = switchclos.SwitchClos(level,position,bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rpcspb.RoutingPolicyClosShortcutPriorityBuffer(s,True))
            return s
        if self.networkType.__eq__('clos-buffer-false-deflection'):
            s = switchclos.SwitchClos(level,position,bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rpcspb.RoutingPolicyClosShortcutPriorityBuffer(s,False))
            return s
        if self.networkType.__eq__('random-routing'):
            if level != None:
                s = switchclos.SwitchClos(level,position,bufferSize)
            else:
                s = switchrandom.SwitchRandom(name,bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rprr.RoutingPolicyRandomRandom(s,False,False))
            return s
        if self.networkType.__eq__('random-deflection'):
            if level != None:
                s = switchclos.SwitchClos(level,position,bufferSize)
            else:
                s = switchrandom.SwitchRandom(name,bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rprr.RoutingPolicyRandomRandom(s,False,True))
            return s
        if self.networkType.__eq__('random-deflection-no-one-loop'):
            s = switchrandom.SwitchRandom(name,bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rprral.RoutingPolicyRandomRandomAvoidLoop(s,False))
            return s
        return None
    
    ''' returns a new copy of a Switch instance '''
    def getCopySwitch(self,switchInstance):
        if self.networkType.__eq__('clique'):
            s=switchclique.SwitchClique(switchInstance.bufferSize)
            s.setName(switchInstance.name)
            return s
        if self.networkType.__eq__('clos-deflection'):
            s = switchclos.SwitchClos(switchInstance.level,switchInstance.position,switchInstance.bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,True,True))
            return s
        if self.networkType.__eq__('clos-no-deflection'):
            s = switchclos.SwitchClos(switchInstance.level,switchInstance.position,switchInstance.bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            return s
        if self.networkType.__eq__('clos-upward-deflection'):
            s = switchclos.SwitchClos(switchInstance.level,switchInstance.position,switchInstance.bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,True,False))
            return s
        if self.networkType.__eq__('clos-smart-deflection'):
            s = switchclos.SwitchClos(switchInstance.level,switchInstance.position,switchInstance.bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rpcsp.RoutingPolicyClosShortcutPriority(s))
            return s
        if self.networkType.__eq__('clos-buffer-true-deflection'):
            s = switchclos.SwitchClos(switchInstance.level,switchInstance.position,switchInstance.bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rpcspb.RoutingPolicyClosShortcutPriorityBuffer(s,True))
            return s
        if self.networkType.__eq__('clos-buffer-false-deflection'):
            s = switchclos.SwitchClos(switchInstance.level,switchInstance.position,switchInstance.bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rpcspb.RoutingPolicyClosShortcutPriorityBuffer(s,False))
            return s
        if self.networkType.__eq__('random-routing'):
            s = switchclos.SwitchClos(switchInstance.level,switchInstance.position,switchInstance.bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rprr.RoutingPolicyRandomRandom(s,False,False))
            return s
        if self.networkType.__eq__('random-deflection'):
            s = switchclos.SwitchClos(switchInstance.level,switchInstance.position,switchInstance.bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rprr.RoutingPolicyRandomRandom(s,False,True))
            return s
        if self.networkType.__eq__('random-deflection-no-one-loop'):
            s = switchrandom.SwitchRandom(switchInstance.bufferSize)
            #s.setRoutingPolicy(rpcsob.RoutingPolicyClosShortcutOutputBuffer(s,False,False))
            s.setRoutingPolicy(rprral.RoutingPolicyRandomRandomAvoidLoop(s,False))
            return s
        