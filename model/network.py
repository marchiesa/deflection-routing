import model.switch as switch


class Network(object):
    
        
    def __init__(self,bufferSize):
        self.nodes =[]
        self.speakers = []
        self.edges = [] 
        self.bufferSize=bufferSize   
    
    def addNode(self,n):
        self.nodes+=[n]
                
    def addNodes(self,nodess):
        self.nodes.extend(nodess)
        
    def addEdge(self,n,m,a):
        if not n in self.nodes:
            raise n()
        if not m in self.nodes:
            raise m()
        else:
            self.edges+=[(n,m,a)]
            n.addEdge(m,a)
            m.addEdge(n,a)
    
    def removeEdge(self,n,m,a):
        self.edges.remove((n,m,a))
        n.removeEdge(m,a)
        m.removeEdge(n,a)
            
    def addEdges(self,lista):
        self.edges.extend(lista)
        for (n,m,a) in lista:
            if not n in self.nodes:
                raise n()
            if not m in self.nodes:
                raise m()
            else:
                n.addEdge(m,a)
                m.addEdge(n,a)
    
    def addSpeaker(self,speaker):
        self.speakers.append(speaker)
        
    def getSpeakers(self):
        return self.speakers
    
    def getNodes(self):
        return self.nodes
    
    def getEdges(self):
        return self.edges
    
    def setBufferSize(self,bufferSize):
        self.bufferSize=bufferSize
        
    def getBufferSize(self):
        return self.bufferSize
    
    def clearBuffers(self):
        for node in self.nodes:
            node.clearBuffers()
            
    
    