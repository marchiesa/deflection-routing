import random
import math
import model.statistics as stat
import model.packet as packetmod


''' This class models a generator of packets for "speaker" nodes '''
''' It generates 'degree' packets at each vertex'''
class TrafficGeneratorFlowCDF:
    
    
        
    ''' initialize a traffic generator for a set of 'nodes' nodes such
        that at most 'maxNumberPacketsPerNode' are generated to a specific node
        and a fraction 'limitInputRate' of the packets are directed to the 
        originator itself '''
    def __init__(self, speakers, maxNumberPacketsPerNode=None, limitInputRate=0,fileName=None):
        
        self.speakers = speakers
        self.maxNumberPacketsPerNode = maxNumberPacketsPerNode
        if maxNumberPacketsPerNode == None:
            self.maxNumberPacketsPerNode = len(speakers)
        self.limitInputRate = limitInputRate
        self.timeStep2packets = {}
        self.source2destination2numberOfPackets = {}
        self.max = None
        #this works only for a specific topology used in the ns3 simulations
        if fileName != None:
            print "yeaaaaaaaaaaah"
            
            speakersImported=[15,16,17,18,19,20,21,22,23,30,31,32,33,34,35,36,37,38,45,46,47,48,49,50,51,52,53,60,61,62,63,64,65,66,67,68,75,76,77,78,79,80,81,82,83,90,91,92,93,94,95,96,97,98]

            for speaker in self.speakers:
                self.source2destination2numberOfPackets[speaker]= {}
            
            f = open(fileName, 'r')
            #data = f.readline()
            #print "data", data
            #data = f.readline()
            #print "data", data
            entry = 0
            degree = 3
            for data in f:
                entry += 1
                if entry % 1000 == 0:
                    print "entry", str(entry/1000)
                if entry == 1:
                    continue
                timeStep = float(data.split(" ")[0]) 
                size= int(data.split(" ")[1])
                source= int(data.split(" ")[2])
                destination= int(data.split(" ")[3])
                
                newTimeStep = long(timeStep*(10**5)*8.4)
                newSize = int(math.ceil(size/1460))
                newSource = self.speakers[speakersImported.index(source)/3]
                newDestination = self.speakers[speakersImported.index(destination) /3]

                if newTimeStep < 210000:
                    self.addFlowOfPackets(newTimeStep,newSize,newSource,newDestination)
                
            f.close()
        
   
    ''' returns a list containing a packet originated by each speaker.
        the set of packets is generated randomly '''
    def generateRandomTraffic(self,limit=False,iteration=None):
        if iteration != None:
            if iteration in self.timeStep2packets.keys():
                for (source,destination,size) in self.timeStep2packets[iteration]:
                    if not destination in self.source2destination2numberOfPackets[source].keys():
                        self.source2destination2numberOfPackets[source][destination]=0
                    self.source2destination2numberOfPackets[source][destination]+=size
        
        for source in self.source2destination2numberOfPackets.keys():
            for _ in range(0,3):
                if len(self.source2destination2numberOfPackets[source].keys()) > 0:
                    randomDestination=random.choice(self.source2destination2numberOfPackets[source].keys())
                    self.source2destination2numberOfPackets[source][randomDestination]-=1
                    if self.source2destination2numberOfPackets[source][randomDestination] == 0:
                         self.source2destination2numberOfPackets[source].pop(randomDestination)
                    #randomDestination=self.speakers[(8*i+3*j+1) % len(self.speakers)]
                    packet = packetmod.Packet(source, randomDestination)
                    #packet.setID(TrafficGeneratorMoreTraffic.id)                    
                    packet.setCreationTime(stat.Statistics().iteration)
                    #print "stattrafiteration:", stat.Statistics().iteration
                    source.addPacketIntoIngressBuffer(packet)
                    stat.Statistics().increaseNumberOfPacketsSentBy(1)
                                        
                                    
    def addFlowOfPackets(self,timeStep,size,source,destination):  
        if not timeStep in self.timeStep2packets.keys():
            self.timeStep2packets[timeStep]=[]
        self.timeStep2packets[timeStep].append([source,destination,size])
        
    def getMaxTime(self):
        if not self.max:
            self.max=0
            for time in self.timeStep2packets.keys():
                if time > self.max:
                    self.max = time 
            return self.max
        else:
            return self.max
        
        
        
        
        
        
        
             