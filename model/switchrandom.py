
import switch

''' 
    This class implements a switch in a ClosNetwork. Refers to 
    model.switch.Switch for more details.  
    - a switch is identified by its x and y cooridinates in the Clos Network.
      the left-bottm vertex is x=y=0. 
'''
class SwitchRandom(switch.Switch):
    
#    def __init__(self,name,size):
#        switch.Switch.__init__(self, name, size)
    
    def __init__(self,name,size):
        switch.Switch.__init__(self, size)
        self.name=str(name)
        self.destination2outgoingLinks={}

    def __hash__(self):
        return self.name.__hash__()
            
    def __eq__(self,other):
        return self.name.__eq__(other.getName())

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name
    
    def setName(self,name):
        self.name=str(name)
        
    def getName(self):
        return self.name
    
    def addOutgoingLinkForDestination(self,link,destination):
        if not destination in self.destination2outgoingLinks.keys():
            self.destination2outgoingLinks[destination]=[]
        self.destination2outgoingLinks[destination].append(link)
        
    def setOutgoingLinksForDestination(self,links,destination):
        self.destination2outgoingLinks[destination]=links
        
    def getOutgoingLinksForDestination(self,destination):
        return self.destination2outgoingLinks[destination]