import random
import model.statistics as stat

''' This class models a generator of packets for "speaker" speakers '''
class TrafficGenerator:
    
    ''' initialize a traffic generator for a set of 'speakers' nodes such
        that at most 'maxNumberPacketsPerNode' are generated to a specific node
        and a fraction 'limitInputRate' of the packets are directed to the 
        originator itself '''
    def __init__(self, speakers, maxNumberPacketsPerNode=None, limitInputRate=0):
        self.speakers = speakers
        self.maxNumberPacketsPerNode = maxNumberPacketsPerNode
        if maxNumberPacketsPerNode == None:
            self.maxNumberPacketsPerNode = len(speakers)
        self.limitInputRate = limitInputRate
                
    ''' returns a list containing a packet originated by each speaker.
        the set of packets is generated randomly '''
    def generateRandomTraffic(self,iteration=None):
        if self.maxNumberPacketsPerNode ==len(self.speakers):
            result = map(lambda x : random.choice(self.speakers[:x]+self.speakers[(x+1):]),range(0,len(self.speakers)))
        else:
            
            ''' first method... it takes 160 seconds on a list with 9000 elements '''
            '''import time
            t=time.time()
            result = [None]*len(self.speakers)
            remainingNodes = self.speakers[:]
            autosenders = range(0,len(self.speakers))
            print "speakers",len(self.speakers)
            for i in autosenders:
                result[i]=random.choice(remainingNodes)
                if result.count(result[i]) == self.maxNumberPacketsPerNode:
                    remainingNodes.remove(result[i])
                if i % 1000 == 0 and i >0:
                    print i,
            print "time",time.time()-t'''
            
            '''second method... it takes 0.06 seconds on a list with 9000 elements!!!'''
            result=self.speakers[:]
            random.shuffle(result)
            for j in range(1,self.maxNumberPacketsPerNode):
                permutation=self.speakers[:]
                random.shuffle(permutation)
                for i in range(0,len(result)):
                    result[i]=random.choice([result[i],permutation[i]])               
        
        stat.Statistics().increaseNumberOfPacketsSentBy(len(result))
        
        ''' apply limitInputRate option '''
        noneSenders = int(self.limitInputRate * len(self.speakers))
        if noneSenders > 0:
            noneIndices = []
            indices = range(0,len(self.speakers))
            for i in range(0,noneSenders):
                value = random.choice(indices)
                noneIndices.append(value)
                indices.remove(value)
            for index in noneIndices:
                #result[index]=self.speakers[index]
                result[index]=None
        #print "result", result
        
        stat.Statistics().decreaseNumberOfPacketsSentBy(noneSenders)
        
        #remove packet where destination = source
        for i in range(0,len(result)):
            if result[i]!=None and result[i].__eq__(self.speakers[i]):
                result[i]=None
                stat.Statistics().decreaseNumberOfPacketsSentBy(1)
        
        #populate buffers        
        for i in range(0,len(result)):
            if result[i] != None:
                self.speakers[i].addPacketIntoIngressBuffer(result[i])
                
        #print "nresult",result 
                 
        return result
                        
                    
            
            