

''' this class models a general packet'''
class Packet(object):
    
    def __init__(self,source=None,destination=None,deflected=False, ttl=0, id=None):
        self.source = source
        self.destination=destination
        self.deflected=deflected
        self.lastHop = source
        self.ttl = ttl
        self.id = id
        
    def __repr__(self):
        if self.getID() != None:
            return 'P('+self.destination.__repr__()+')' + str(self.deflected)+","+str(self.getID())
        else:
            return 'P('+self.destination.__repr__()+')' + str(self.deflected)
    
    def __str__(self):
        if self.getID() != None:
            return 'P('+self.destination.__repr__()+')' + str(self.deflected)+","+str(self.getID())
        else:
            return 'P('+self.destination.__repr__()+')' + str(self.deflected)
        
    def setDestination(self,destination):
        self.destination=destination
        
    def getDestination(self):
        return self.destination
    
    def getSource(self):
        return self.source
    
    def markAsDeflected(self):
        self.deflected=True
    
    def markAsNonDeflected(self):
        self.deflected=False
        
    def isDeflected(self):
        return self.deflected
    
    def getLastHop(self):
        return self.lastHop
    
    def reduceTTL(self):
        self.ttl -= 1
        return self.ttl
    
    def setCreationTime(self,time):
        self.time=time
        
    def getCreationTime(self):
        return self.time
    
    def setID(self,id):
        self.id =id
        
    def getID(self):
        return self.id
        
