
import model.routingpolicy as rp
import random 
import model.statistics as stat

'''
    This class implements a Routing Policy for a Switch in a Clos network
    without ever sending a packet backwards. Each packet is sent upwards
    to a top level switch and then downwards to the destination. Observe that, 
    if each speaker generates a vertex, then deflecting a packet backwards 
    will result in a drop. 
    - all packets that must be sent upwards are randomly mapped to any upward
      neighbor.
    - all packets that must be sent downwards have a unique neighbor. Contention
      may happen and is resolved by dropping packets.
'''
class RoutingPolicyClosShortcut(rp.RoutingPolicy):
    
    def __init__(self,switch,upwardDeflection=True,downwardDeflection=True):
        rp.RoutingPolicy.__init__(self,switch)
        self.upwardDeflection=upwardDeflection
        self.downwardDeflection=downwardDeflection
        
    def enableDeflection(self):
        self.upwardDeflection=True
        self.downwardDeflection=True
        
    def disableDeflection(self):
        self.upwardDeflection=False
        self.downwardDeflection=False
    
    def enableUpwardDeflection(self):
        self.upwardDeflection=True
        
    def disableUpwardDeflection(self):
        self.upwardDeflection=False
    
    def enableDownwardDeflection(self):
        self.downwardDeflection=True
        
    def disableDownwardDeflection(self):
        self.downwardDeflection=False
            
    ''' processes incoming packets and returns a map 'interface name' -> packet 
        it always sends a packet upward to a toplevel switch and then downward to the destination '''   
    def processPackets(self):
        output = {}
        
        aboveEdges=self.switch.getEdgesAboveLevel()
        belowEdges=self.switch.getEdgesLowerLevel()
        freeAboveEdges=aboveEdges[:]
        freeBelowEdges=belowEdges[:]           
        degree= len(aboveEdges)
        if degree == 0:
            degree=len(belowEdges) 

        
        #print "i'm",self.switch,"no top no speaker"
        allEdges=belowEdges[:]
        allEdges.extend(aboveEdges)
            
        while len(self.switch.getIngress()) > 0:
            packet = self.switch.getIngress().popleft()
            if packet.getDestination().__eq__(self.switch):
                #packet arrived to destination
                print self.switch, "received packet for myself"
                stat.Statistics().increaseSuccessByOne()
                #print "getSuccess",stat.Statistics().getSuccess()
                #self.sendRandomUpwardOnlyOnAvailableEdges(freeAboveEdges, output, packet)
            else: 
                self.sendRandomUpwardOnlyOnAvailableEdges(freeAboveEdges, output, packet)
            
        for edge in allEdges:
            if len(self.switch.getPorts()[edge]) > 0:
                packet = self.switch.getPorts()[edge].popleft()
                #print "switch:",self.switch,"packet to",packet
                if packet.__eq__(self.switch):
                    #packet arrived to destination
                    #print self.switch, "received packet for myself"
                    stat.Statistics().increaseSuccessByOne()
                    #self.sendRandomUpwardOnlyOnAvailableEdges(freeAboveEdges, output, packet)
                    continue
                sent=False
                if self.isPacketDirectedDownward(degree, packet, belowEdges):
                    sent=self.sendDownward(degree, freeBelowEdges, output, packet)  
                    if not sent and not (self.upwardDeflection and self.downwardDeflection):
                        stat.Statistics().increaseDropByOne()
                        continue                      
                if not sent:
                    if self.upwardDeflection:
                        if not self.sendRandomUpwardOnlyOnAvailableEdges(freeAboveEdges, output, packet):
                            #if self.isPacketDirectedDownward(degree, packet, belowEdges): 
                                #print "************* DEFLECTION!! packet",packet,"at",self.switch
                            if self.downwardDeflection:
                                if not self.sendRandomDownward(freeBelowEdges, output, packet):
                                    #print "************* HERE!!", freeBelowEdges,freeAboveEdges,self.switch,packet,output
                                    stat.Statistics().increaseDropByOne()
                    else:
                        if not self.sendRandomUpward(aboveEdges, freeAboveEdges, output, packet):
                            stat.Statistics().increaseDropByOne()                         
                                               
        return output
            
    def isPacketDirectedDownward(self,degree,destination,belowEdges):
        level = self.switch.getLevel()
        position = self.switch.getPosition() 
        widthPod = degree**(level)
        #print "level",level,"position",position,"widthPod",widthPod, "destination",destination,"degree",degree, "belowEdges",belowEdges
        pod = position / widthPod
        return pod == destination.position/widthPod
        #return len(filter(lambda (a,n): (n.position-(pod*widthPod))/widthSubPod== (destination.position-(pod*widthPod))/widthSubPod,belowEdges))!=0
     
    ''' it tries to send a packet upward to a random edge. If there is a contention, it fails.'''
    def sendRandomUpward(self,aboveEdges,freeAboveEdges,output,packet):
        edge =random.choice(aboveEdges)
        if edge in freeAboveEdges:
            return False
        else:
            aboveEdges.remove(edge)
            output[edge]=packet
            return True

    ''' it tries to send a packet upward to a random available edge. If it does not exists, it fails 
        but it does not drop the packet'''
    def sendRandomUpwardOnlyOnAvailableEdges(self,freeAboveEdges,output,packet):
        if len(freeAboveEdges) >0:
            edge =random.choice(freeAboveEdges)
            freeAboveEdges.remove(edge)
            output[edge]=packet
            return True
        else:
            return False
            
    def sendRandomDownward(self,freeBelowEdges,output,packet):
        if len(freeBelowEdges) >0:
            edge =random.choice(freeBelowEdges)
            freeBelowEdges.remove(edge)
            output[edge]=packet
            return True
        else:
            return False
    
    def sendDownward(self,degree,freeBelowEdges,output,packet):
        if len(freeBelowEdges)>0:
            level = self.switch.getLevel()
            position = self.switch.getPosition() 
            widthPod = degree**(level)
            #print "level",level,"position",position,"widthPod",widthPod, "packet",packet,"degree",degree
            pod = position / widthPod
            widthSubPod = widthPod/degree
            #for (a,n) in freeBelowEdges:
            #    print "op1",(int(n.name.split('_')[1])-(pod*widthPod))/widthSubPod,"op2",(int(packet.name.split('_')[1])-(pod*widthPod))/widthSubPod                              
            edges = filter(lambda (a,n): (n.position-(pod*widthPod))/widthSubPod== (packet.position-(pod*widthPod))/widthSubPod,freeBelowEdges)
            if len(edges) == 0:
                return False
            else:
                edge = edges[0]
                freeBelowEdges.remove(edge)
                output[edge]=packet
                return True
        else:
            return False
            