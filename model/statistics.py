
import model.singleton as singleton

class Statistics(singleton.Singleton,object):
    
    countDrop=0
    countSuccess=0
    countSuccessUnmarked=0
    countSuccessMarked=0
    numberOfPacketsSent=0
    iteration=0 
    writer= None
    space2numberOfSwitches = {}
    numberMarked2numberOfSwitches = {}
    numberUnmarked2numberOfSwitches = {}
        
    def resetStatistics(self):
        self.countDrop=0
        self.countSuccess=0
        self.countSuccessUnmarked=0
        self.countSuccessMarked=0
        self.numberOfPacketsSent=0
        self.delay2numberOfUnmarkedPackets={}
        self.delay2numberOfMarkedPackets={}
        self.delay2numberOfPackets={}
        self.LIhops2numberOfPackets={}
        self.iteration=0 
        self.writer= None
        self.space2numberOfSwitches = {}
        self.numberMarked2numberOfSwitches = {}
        self.numberUnmarked2numberOfSwitches = {}
        self.averageLinkUtilization = 0
        self.LIhops2numberOfMarkedPackets ={}
        self.LIhops2numberOfUnmarkedPackets = {}
            
    def cleanForNewIteration(self):
        #Last Iteration values
        self.LIdelaySum=long(0)
        self.LIdelayMarkedSum=long(0)
        self.LIdelayUnmarkedSum=long(0)
        self.LIreceivedMarkedPackets=0
        self.LIreceivedUnmarkedPackets=0
        self.LIdropPackets=0
        self.LIsentPackets=0
        self.LIdelay2numberOfUnmarkedPackets={}
        self.LIdelay2numberOfMarkedPackets={}
        self.LIdelay2numberOfPackets={}
        self.space2numberOfSwitches = {}
        self.numberMarked2numberOfSwitches = {}
        self.numberUnmarked2numberOfSwitches = {}
        self.averageLinkUtilization = 0
        self.LIhops2numberOfPackets={}
        self.LIhops2numberOfMarkedPackets ={}
        self.LIhops2numberOfUnmarkedPackets = {}
        
    def increaseNumberOfPacketsSentBy(self,amount=1):
        self.numberOfPacketsSent+=amount
        self.LIsentPackets+=amount
        
    def decreaseNumberOfPacketsSentBy(self,amount=1):
        self.numberOfPacketsSent-=amount
        
    def increaseDropByOne(self):
        self.countDrop+=1
        self.LIdropPackets+=1
        
    def increaseSuccessByOne(self):
        self.countSuccess+=1
        self.receivedPackets+=1
      
    def increaseMarkedSuccessByOne(self):
        self.countSuccessUnmarked+=1
    
    def increaseUnmarkedSuccessByOne(self):
        self.countSuccessMarked+=1
      
    def getNumberOfPacketsSent(self):
        return self.numberOfPacketsSent
    
    def getDrop(self):
        return self.countDrop
    
    def getSuccess(self):
        return self.countSuccess
    
    def getMarkedSuccess(self):
        return self.countSuccessMarked
    
    def getUnmarkedSuccess(self):
        return self.countSuccessUnmarked
    
    def getDropRatio(self):
        return float(self.countDrop)/(self.countDrop+self.countSuccess)
    
    def setIteration(self,iteration):
        self.iteration=iteration
        
    def getIteration(self):
        return self.iteration
    
    def recordDelay(self,delay,isDeflected):
        if isDeflected:
            if not delay in self.LIdelay2numberOfMarkedPackets.keys():
                self.LIdelay2numberOfMarkedPackets[delay]=0
            self.LIdelay2numberOfMarkedPackets[delay]=self.LIdelay2numberOfMarkedPackets[delay]+1
        else:
            if not delay in self.LIdelay2numberOfUnmarkedPackets.keys():
                self.LIdelay2numberOfUnmarkedPackets[delay]=0
            self.LIdelay2numberOfUnmarkedPackets[delay]=self.LIdelay2numberOfUnmarkedPackets[delay]+1
        if not delay in self.LIdelay2numberOfPackets.keys():
                self.LIdelay2numberOfPackets[delay]=0
        self.LIdelay2numberOfPackets[delay]=self.LIdelay2numberOfPackets[delay]+1
           
    def recordHops(self,packet,isDeflected):
        hops = -packet.ttl
        if isDeflected:
            if not hops in self.LIhops2numberOfMarkedPackets.keys():
                self.LIhops2numberOfMarkedPackets[hops]=0
            self.LIhops2numberOfMarkedPackets[hops]=self.LIhops2numberOfMarkedPackets[hops]+1
        else:
            if not hops in self.LIhops2numberOfUnmarkedPackets.keys():
                self.LIhops2numberOfUnmarkedPackets[hops]=0
            self.LIhops2numberOfUnmarkedPackets[hops]=self.LIhops2numberOfUnmarkedPackets[hops]+1
        if not hops in self.LIhops2numberOfPackets.keys():
                self.LIhops2numberOfPackets[hops]=0
        
        if not hops in self.LIhops2numberOfPackets.keys():
            self.LIhops2numberOfPackets[hops]=0
        self.LIhops2numberOfPackets[hops]=self.LIhops2numberOfPackets[hops]+1
        #print "hops",hops,self.LIhops2numberOfPackets[hops]
    
    def getLastIterationHops(self):
        return self.LIhops2numberOfPackets        
    
    def getLastIterationHopsUnmarked(self):
        return self.LIhops2numberOfUnmarkedPackets   
    
    def getLastIterationHopsMarked(self):
        return self.LIhops2numberOfMarkedPackets 
        
    def getLastIterationDelays(self):
        return self.LIdelay2numberOfPackets
    
    def getLastIterationDelaysUnmarked(self):
        return self.LIdelay2numberOfUnmarkedPackets
    
    def getLastIterationDelaysMarked(self):
        return self.LIdelay2numberOfMarkedPackets
        
    def recordSuccess(self,packet):
        self.countSuccess+=1
        #print packet.getCreationTime(), "-",self.getIteration(),"=",packet.getCreationTime()-self.getIteration()
        if packet.isDeflected():
            self.countSuccessMarked+=1
            self.LIreceivedMarkedPackets+=1
            self.LIdelayMarkedSum +=  packet.getCreationTime()-self.getIteration()
            if packet.getCreationTime()-self.getIteration() < 0:
                None.fail()
        else:
            self.countSuccessUnmarked+=1
            self.LIreceivedUnmarkedPackets+=1
            self.LIdelayUnmarkedSum += packet.getCreationTime()-self.getIteration()
        self.recordDelay(packet.getCreationTime()-self.getIteration(),packet.isDeflected())
        self.recordHops(packet,packet.isDeflected())
        self.LIdelaySum += packet.getCreationTime()-self.getIteration()
        
    def getLastIterationAverageDelay(self):
        if self.LIreceivedMarkedPackets+self.LIreceivedUnmarkedPackets == 0:
            return 0
        else:
            return self.LIdelaySum/float(self.LIreceivedMarkedPackets+self.LIreceivedUnmarkedPackets)
    
    def getLastIterationAverageUnmarkedDelay(self):
        if self.LIreceivedUnmarkedPackets == 0:
            return None
        else:
            return self.LIdelayUnmarkedSum/float(self.LIreceivedUnmarkedPackets)
    
    def getLastIterationAverageMarkedDelay(self):
        if self.LIreceivedMarkedPackets == 0:
            return None
        else:
            return self.LIdelayMarkedSum/float(self.LIreceivedMarkedPackets)
    
    def getLastIterationDrop(self):
        return  self.LIdropPackets
    
    def getLastIterationNumberOfPacketsSent(self):
        return self.LIsentPackets
    
    def getLastIterationSuccess(self):
        return self.LIreceivedMarkedPackets+self.LIreceivedUnmarkedPackets

    def getLastIterationUnmarkedSuccess(self):
        return self.LIreceivedUnmarkedPackets
    
    def getLastIterationMarkedSuccess(self):
        return self.LIreceivedMarkedPackets
    
    def recordBuffers(self,network):
        sumPackets = long(0)
        count=float(0)
        bufferSize=network.getBufferSize()
        for i in range(0,bufferSize+1):
            self.space2numberOfSwitches[i]=0
            
        for node in network.getNodes():
            for edge in node.getPorts().keys():
                
                numberUnmarked = len(filter(lambda x: not x.isDeflected(),node.getPorts()[edge]))
                '''if not numberUnmarked in self.numberUnmarked2numberOfSwitches.keys():
                    self.numberUnmarked2numberOfSwitches[numberUnmarked]=0
                self.numberUnmarked2numberOfSwitches[numberUnmarked]+=1'''
                
                numberMarked = len(filter(lambda x: x.isDeflected(),node.getPorts()[edge]))
                '''if not numberMarked in self.numberMarked2numberOfSwitches.keys():
                    self.numberMarked2numberOfSwitches[numberMarked]=0
                self.numberMarked2numberOfSwitches[numberMarked]+=1'''
                
                total=numberUnmarked+numberMarked
                sumPackets+=total/network.getBufferSize()
                count+=1
                if not total in self.space2numberOfSwitches.keys():
                    self.space2numberOfSwitches[total]=0
                self.space2numberOfSwitches[total]+=1
                
        self.averageLinkUtilization =  sumPackets/count
    
    def getSpace2NumberOfSwitches(self):
        return self.space2numberOfSwitches
    
    def getNumberUnmarked2numberOfSwitches(self):
        return self.numberUnmarked2numberOfSwitches
    
    def getNumberMarked2numberOfSwitches(self):
        return self.numberMarked2numberOfSwitches
    
    def getAverageLinkUtilization(self):
        return self.averageLinkUtilization
    


