import random

''' NOT YET IMPLEMENTED'''

class TrafficGenerator:
    
    def __init__(self, speakers, maxNumberPacketsPerNode=None, limitInputRate=0):
        self.speakers = speakers
        self.maxNumberPacketsPerNode = maxNumberPacketsPerNode
        if maxNumberPacketsPerNode == None:
            self.maxNumberPacketsPerNode = len(speakers)
        self.limitInputRate = limitInputRate
                
    def generateRandomTraffic(self,iteration=None):
        if self.maxNumberPacketsPerNode ==len(self.speakers):
            result = map(lambda x : random.choice(self.speakers[:x]+self.speakers[(x+1):]),range(0,len(self.speakers)))
        else:
            
            #first method... it takes 160 seconds on a list with 9000 elements
            '''import time
            t=time.time()
            result = [None]*len(self.speakers)
            remainingNodes = self.speakers[:]
            autosenders = range(0,len(self.speakers))
            print "speakers",len(self.speakers)
            for i in autosenders:
                result[i]=random.choice(remainingNodes)
                if result.count(result[i]) == self.maxNumberPacketsPerNode:
                    remainingNodes.remove(result[i])
                if i % 1000 == 0 and i >0:
                    print i,
            print "time",time.time()-t'''
            
            #second method... it takes 0.06 seconds on a list with 9000 elements!!!
            result=self.speakers[:]
            random.shuffle(result)
            for i in range(1,self.maxNumberPacketsPerNode):
                permutation=self.speakers[:]
                random.shuffle(permutation)
                for i in range(0,len(result)):
                    result[i]=random.choice([result[i],permutation[i]])               
        
        selfSenders = int(self.limitInputRate * len(self.speakers))
        if selfSenders > 0:
            selfIndices = []
            indices = range(0,len(self.speakers))
            for i in range(0,selfSenders):
                value = random.choice(indices)
                selfIndices.append(value)
                indices.remove(value)
            for index in selfIndices:
                result[index]=self.speakers[index]
        print "result", result
                 
        return result
                        
                    
            
            