
import model.routingpolicy as rp
import random 
import model.statistics as stat
from collections import deque

'''
    This class implements a Routing Policy for a Switch in a Random network
    It simply sends packet at random
'''
class RoutingPolicyRandomRandom(rp.RoutingPolicy):
    
    test=False
    def __init__(self,switch,type=True,deflection=True):
        rp.RoutingPolicy.__init__(self,switch)
        self.type=type
        self.deflection=deflection
        
    ''' processes incoming packets and returns a map 'interface name' -> packet 
        it always sends a packet upward to a toplevel switch and then downward to the destination '''   
    def processPackets(self,packets):
        output = {}
        
        
        
        edges=self.switch.getPorts().keys()
        freeMarkedEdges=filter(lambda e : len(self.switch.getPorts()[e]) < self.switch.getBufferSize(),edges[:])
        freeUnmarkedEdges=filter(lambda e : len(filter(lambda p : p.isDeflected(),self.switch.getPorts()[e])) > 0 or len(self.switch.getPorts()[e]) < self.switch.getBufferSize(),edges[:])
             
        packetsKeys= packets.keys()
        random.shuffle(packetsKeys)
        
        edgeWithDownwardDeflectedPackets =filter(lambda x : packets[x].isDeflected(),packetsKeys)
        edgeWithNonDownwardDeflectedPackets =filter(lambda x : not packets[x].isDeflected(),packetsKeys)
                 
                 
        #print "**i'm",self.switch, "with buffer", self.switch.getBufferSize(),"len", len(self.switch.getIngress())
            
        deflected=[]
        while len(self.switch.getIngress()) > 0:
            packet = self.switch.getIngress().popleft()
            if packet.getDestination().__eq__(self.switch):
                #packet arrived to destination
                ##print self.switch, "received packet for myself"
                stat.Statistics().recordSuccess(packet)
                ##print "getSuccess",stat.Statistics().getSuccess()
                #self.sendPacket(freeAboveEdges, output, packet)
            else:
                #print " sending packet", packet, "to", packet.getDestination() 
                sent=self.sendPacket(freeMarkedEdges,freeUnmarkedEdges, output, packet, edges)
                if not sent:
                    #print "notsent"
                    if self.deflection:
                        packet.markAsDeflected()
                        deflected.append(packet)
                    else:
                        stat.Statistics().increaseDropByOne()
                
        for packet in deflected:
            self.switch.addPacketIntoIngressBuffer(packet)

        ##print "i'm", self.switch
        #for (a,n) in allEdges:
        for edge in edgeWithNonDownwardDeflectedPackets:
            ##print "edge", (a,n)
            #if len(n.getPorts()[(a,self.switch)]) > 0:
            ##print "pkts", n.getPorts()[(a,self.switch)]
            packet =packets[edge]
            #print " sending packet", packet, "to", packet.getDestination()
            if packet.getDestination().__eq__(self.switch):
                #packet arrived to destination
                ##print self.switch, "received packet for myself"
                stat.Statistics().recordSuccess(packet)
                #self.sendPacket(freeAboveEdges, output, packet)
                continue
            sent=False
            
            sent=self.sendPacket(freeMarkedEdges,freeUnmarkedEdges, output, packet, edges)
            
            if not sent:
                if self.deflection:
                    packet.markAsDeflected()
                    edgeWithDownwardDeflectedPackets.append(edge)
                    continue
                else:
                    stat.Statistics().increaseDropByOne()                  
        
        #send deflected packets from ingress buffer
        while len(self.switch.getIngress()) > 0:
            packet = self.switch.getIngress().popleft()
            if packet.getDestination().__eq__(self.switch):
                #packet arrived to destination
                ##print self.switch, "received packet for myself"
                stat.Statistics().recordSuccess(packet)
                ##print "getSuccess",stat.Statistics().getSuccess()
                #self.sendPacket(freeAboveEdges, output, packet)
            else:
                #print " sending  deflected packet", packet, "to", packet.getDestination() 
                sent=self.sendDeflectedPacket(freeMarkedEdges,freeUnmarkedEdges, output, packet, edges)
                
                
                          
        for edge in edgeWithDownwardDeflectedPackets:
            ##print "edge", (a,n)
            #if len(n.getPorts()[(a,self.switch)]) > 0:
            ##print "pkts", n.getPorts()[(a,self.switch)]
            packet =packets[edge]
            #stat.Statistics().increaseDropByOne()
            #continue
            ##print "packet to",packet, "from", edge
            ##print "hoy"
            self.sendDeflectedPacket(freeMarkedEdges,freeUnmarkedEdges, output, packet, edges)
           
        ##print "ports", self.switch.getPorts()                                   
        return output
        
        
    def sendDeflectedPacket(self,freeMarkedEdges,freeUnmarkedEdges, output, packet, edges):
        #stat.Statistics().increaseDropByOne()
        #continue
        #print "here"
        if packet.getDestination().__eq__(self.switch):
            #packet arrived to destination
            ##print self.switch, "received packet for myself"
            stat.Statistics().recordSuccess(packet)
            #self.sendPacket(freeAboveEdges, output, packet)
            return
        
        #print " sending deflected packet", packet, "to", packet.getDestination()
        #try to send a deflected path on the correct path 
        sent=self.sendPacket(freeMarkedEdges,freeUnmarkedEdges, output, packet, edges)
        #print "sent", sent
        #try to deflect this packet
        if not sent:
            sent=self.sendPacketOnDeflectedRoute(freeMarkedEdges,freeUnmarkedEdges, output, packet, edges)
            if not sent:
                stat.Statistics().increaseDropByOne()
                return False
            else :
                return True 
                    
    def sendPacket(self,freeMarkedEdges,freeUnmarkedEdges, output, packet, edges):
        if packet.isDeflected():
            freeEdges=freeMarkedEdges
        else:
            freeEdges=freeUnmarkedEdges
        
        if len(freeEdges)>0:
            ##print "i'm", self.switch, "trying to send packet", packet
            goodEdges=filter(lambda x : x in freeEdges,self.switch.getOutgoingLinksForDestination(packet.getDestination()))
            
            if len(goodEdges) == 0:
                return False
            
            edge = random.choice(goodEdges) #edge is (attribute,neighbor)
            #edge = goodEdges[0]
            
            #edge = self.switch.getOutgoingLinkForDestination(packet.getDestination()) #edge is (attribute,neighbor)
            
            if not edge in freeEdges:
                packet.markAsDeflected()
                ##print "packet",packet,"is marked as deflected"
                ##print "#####output",output
                return False
            else:
                if not edge in output.keys():
                    output[edge]=deque([])
                #print "plan to send packet",packet,"to",edge
                #print "  buffer",self.switch.getPorts()[edge]
                #print "  output",output[edge]
                #if len(self.switch.getPorts()[edge]) > 0:
                #    if self.switch.getPorts()[edge][0].isDeflected():
                #        if len(self.switch.getPorts()[edge])+len(output[edge]) >= self.switch.getBufferSize():
                #            #print "len",len(self.switch.getPorts()[edge]), "output", output[edge], "len", len(output[edge])
                
                if len(self.switch.getPorts()[edge]) +1 +len(output[edge])== self.switch.getBufferSize():
                    ##print "remove", edge 
                    if edge in freeMarkedEdges:
                        freeMarkedEdges.remove(edge)
                    pd = filter(lambda p : p.isDeflected(),self.switch.getPorts()[edge])
                    pdOutput = filter(lambda p : p.isDeflected(),output[edge])
                    lenpd = len(pd) + len(pdOutput)
                    if lenpd == 0 and not packet.isDeflected():
                        ##print "******remove"
                        freeUnmarkedEdges.remove(edge)
                
                if not packet.isDeflected() and len(self.switch.getPorts()[edge]) +1 +len(output[edge])> self.switch.getBufferSize():        
                    pd = filter(lambda p : p.isDeflected(),self.switch.getPorts()[edge])
                    lenpd = len(pd)
                    if len(pd)==0:
                        pd = filter(lambda p : p.isDeflected(),output[edge])
                    pck = pd[len(pd)-1]
                    ##print "buffer", self.switch.getPorts()[edge]
                    ##print "output", output[edge]
                    ##print "removed packet", pck
                    if pck in self.switch.getPorts()[edge]:
                        self.switch.getPorts()[edge].remove(pck)
                    else:
                        output[edge].remove(pck)
                    #print "rerouting",pck
                    self.sendDeflectedPacket(freeMarkedEdges,freeUnmarkedEdges, output, pck, edges)
                    #print " pd",pd
                    if len(pd) == 1:
                        freeUnmarkedEdges.remove(edge)
                        #print " removed",edge
                    #print " freeBelow",freeUnmarkedEdges

                output[edge].append(packet)
                #print "send packet",packet,"to",edge
                #print "  buffer",self.switch.getPorts()[edge]
                #print "  output",output[edge]
                #print "----"
                ##print "packet",packet,"not marked as deflected",packet.isDeflected(), "output", output[edge], "---",output
                return True
        else:
            packet.markAsDeflected()
            ##print "packet",packet,"is marked as deflected"
            ##print "#####output",output
            return False           
        
    
   
    ''' it tries to send a packet on its correct path'''
    def sendPacketOnDeflectedRoute(self,freeMarkedEdges,freeUnmarkedEdges, output, packet, edges):
        if packet.isDeflected():
            freeEdges=freeMarkedEdges
            #print "1freeAbove", freeEdges
            #print "          ", freeUnmarkedEdges
            #print "          ", freeMarkedEdges
        else:
            if self.type:
                if len(freeMarkedEdges) >0:
                    freeEdges=freeMarkedEdges
                else:
                    freeEdges=freeUnmarkedEdges
            else:
                freeEdges=freeUnmarkedEdges
                #print "1freeAbove", freeEdges
                #print "          ", freeUnmarkedEdges
                #print "          ", freeMarkedEdges
        
        if len(freeEdges) >0:
            #print "2freeAbove", freeEdges
            #print "          ", freeUnmarkedEdges
            #print "          ", freeMarkedEdges
            
            #edge =freeEdges[0]
            edge =random.choice(freeEdges)
            #edge = freeEdges[0]
            
            ##print "keys",output.keys()
            if not edge in output.keys():
                ##print "add key",edge,"to output"
                output[edge]=deque([])
            #print "plan to send packet",packet,"to",edge
            #print "  buffer",self.switch.getPorts()[edge]
            #print "  output",output[edge]
            ##print "free", freeEdges, "ports",self.switch.getPorts().keys()
            if len(self.switch.getPorts()[edge]) +1 +len(output[edge])== self.switch.getBufferSize():
                ##print "remove", edge 
                #print "3freeAbove", freeEdges
                #print "          ", freeUnmarkedEdges
                #print "          ", freeMarkedEdges
                freeMarkedEdges.remove(edge)
                #print "4freeAbove", freeEdges
                #print "          ", freeUnmarkedEdges
                #print "          ", freeMarkedEdges
                pd = filter(lambda p : p.isDeflected(),self.switch.getPorts()[edge])
                pdOutput = filter(lambda p : p.isDeflected(),output[edge])
                lenpd = len(pd) + len(pdOutput)
                if lenpd == 0 and not packet.isDeflected():
                    freeUnmarkedEdges.remove(edge)
            
            output[edge].append(packet)
            #print "send packet",packet,"to",edge
            #print "  buffer",self.switch.getPorts()[edge]
            #print "  output",output[edge]
            #print "----"
            return True
        else:
            packet.markAsDeflected()
            return False
            
            