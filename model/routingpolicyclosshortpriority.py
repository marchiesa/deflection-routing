
import model.routingpolicy as rp
import random 
import model.statistics as stat
from collections import deque

'''
    This class implements a Routing Policy for a Switch in a Clos network
    without ever sending a packet backwards. Each packet is sent upwards
    to a top level switch and then downwards to the destination. Observe that, 
    if each speaker generates a vertex, then deflecting a packet backwards 
    will result in a drop. 
    - all packets that must be sent upwards are randomly mapped to any upward
      neighbor.
    - all packets that must be sent downwards have a unique neighbor. Contention
      may happen and is resolved by dropping packets.
'''
class RoutingPolicyClosShortcutPriority(rp.RoutingPolicy):
    
    def __init__(self,switch):
        rp.RoutingPolicy.__init__(self,switch)
        
    ''' processes incoming packets and returns a map 'interface name' -> packet 
        it always sends a packet upward to a toplevel switch and then downward to the destination '''   
    def processPackets(self,packets):
        output = {}
        
        aboveEdges=self.switch.getEdgesAboveLevel()
        belowEdges=self.switch.getEdgesLowerLevel()
        freeAboveEdges=filter(lambda e : len(self.switch.getPorts()[e]) < self.switch.getBufferSize(),aboveEdges[:])
        freeBelowEdges=filter(lambda e : len(self.switch.getPorts()[e]) < self.switch.getBufferSize(),belowEdges[:])          
        degree= len(aboveEdges)
        if degree == 0:
            degree=len(belowEdges) 

            
        
        #print "i'm",self.switch,"no top no speaker"
        allEdges=belowEdges[:]
        allEdges.extend(aboveEdges)
            
        #print "**i'm",self.switch, "with buffer", self.switch.getBufferSize()
            
        while len(self.switch.getIngress()) > 0:
            packet = self.switch.getIngress().popleft()
            if packet.getDestination().__eq__(self.switch):
                #packet arrived to destination
                #print self.switch, "received packet for myself"
                stat.Statistics().recordSuccess(packet)
                #print "getSuccess",stat.Statistics().getSuccess()
                #self.sendPacket(freeAboveEdges, output, packet)
            else: 
                self.sendPacket(freeAboveEdges, output, packet)
         
        edgeWithDownwardDeflectedPackets =filter(lambda x : packets[x].isDeflected(),packets.keys())
        edgeWithNonDownwardDeflectedPackets =filter(lambda x : not packets[x].isDeflected(),packets.keys())
                 
        #print "i'm", self.switch
        #for (a,n) in allEdges:
        for edge in edgeWithNonDownwardDeflectedPackets:
            #print "edge", (a,n)
            #if len(n.getPorts()[(a,self.switch)]) > 0:
            #print "pkts", n.getPorts()[(a,self.switch)]
            packet =packets[edge]
            #print "packet to",packet, "from", (a,n)
            if packet.getDestination().__eq__(self.switch):
                #packet arrived to destination
                #print self.switch, "received packet for myself"
                stat.Statistics().recordSuccess(packet)
                #self.sendPacket(freeAboveEdges, output, packet)
                continue
            sent=False
            
            isDirectedDownward = self.isPacketDirectedDownward(degree, packet.getDestination(), belowEdges)
            sent=False
            if isDirectedDownward:
                sent=self.sendDownward(degree, freeBelowEdges, output, packet)  
                if not sent:
                    edgeWithDownwardDeflectedPackets.append(edge)
                    continue                  
            if not sent or not isDirectedDownward :
                if not self.sendPacket(freeAboveEdges, output, packet):
                    edgeWithDownwardDeflectedPackets.append(edge)
                    continue
            
            
            '''if self.isPacketDirectedDownward(degree, packet.getDestination(), belowEdges):
                sent=self.sendDownward(degree, freeBelowEdges, output, packet)  
                if not sent:
                    if not self.sendPacket(freeAboveEdges, output, packet):
                        edgeWithDownwardDeflectedPackets.append(edge)
                continue
            if not self.sendPacket(freeAboveEdges, output, packet):
                edgeWithDownwardDeflectedPackets.append(edge)'''
            
        for edge in edgeWithDownwardDeflectedPackets:
            #stat.Statistics().increaseDropByOne()
            #continue
            #print "edge", (a,n)
            #if len(n.getPorts()[(a,self.switch)]) > 0:
            #print "pkts", n.getPorts()[(a,self.switch)]
            packet =packets[edge]
            if not packet.isDeflected():
                None.expand([])
            #print "packet to",packet, "from", (a,n)
            if packet.getDestination().__eq__(self.switch):
                #packet arrived to destination
                #print self.switch, "received packet for myself"
                stat.Statistics().recordSuccess(packet)
                #self.sendPacket(freeAboveEdges, output, packet)
                continue
            sent=False
            if self.isPacketDirectedDownward(degree, packet.getDestination(), belowEdges):
                sent=self.sendDownward(degree, freeBelowEdges, output, packet)  
            if not sent:
                if not self.sendPacket(freeAboveEdges, output, packet):
                    #if self.isPacketDirectedDownward(degree, packet, belowEdges): 
                        #print "************* DEFLECTION!! packet",packet,"at",self.switch
                    if not self.sendRandomDownward(freeBelowEdges, output, packet):
                        #print "************* HERE!!", freeBelowEdges,freeAboveEdges,self.switch,packet,output
                        stat.Statistics().increaseDropByOne()
                        #print "i'm", self.switch, "available", freeAboveEdges,"edge",edge
            
        
        #print "ports", self.switch.getPorts()                                   
        return output
            
    def isPacketDirectedDownward(self,degree,destination,belowEdges):
        level = self.switch.getLevel()
        position = self.switch.getPosition() 
        widthPod = degree**(level)
        #print "level",level,"position",position,"widthPod",widthPod, "destination",destination,"degree",degree, "belowEdges",belowEdges
        pod = position / widthPod
        return pod == destination.position/widthPod
        #return len(filter(lambda (a,n): (n.position-(pod*widthPod))/widthSubPod== (destination.position-(pod*widthPod))/widthSubPod,belowEdges))!=0
     
    ''' it tries to send a packet upward to a random available edge. If it does not exists, it fails 
        but it does not drop the packet'''
    def sendPacket(self,freeAboveEdges,output,packet):
        if len(freeAboveEdges) >0:
            edge =random.choice(freeAboveEdges)
            #print "keys",output.keys()
            if not edge in output.keys():
                #print "add key",edge,"to output"
                output[edge]=deque([])
            #print "free", freeAboveEdges, "ports",self.switch.getPorts().keys()
            if len(self.switch.getPorts()[edge]) +1 +len(output[edge])== self.switch.getBufferSize():
                #print "remove", edge 
                freeAboveEdges.remove(edge)
            #print "output", output[edge]
            output[edge].append(packet)
            #print "output", output[edge]
            return True
        else:
            packet.markAsDeflected()
            return False
            
    def sendRandomDownward(self,freeBelowEdges,output,packet):
        if len(freeBelowEdges) >0:
            edge =random.choice(freeBelowEdges)
            if not edge in output.keys():
                output[edge]=deque([])
            if len(self.switch.getPorts()[edge]) +1 +len(output[edge])== self.switch.getBufferSize(): 
                freeBelowEdges.remove(edge)
            output[edge].append(packet)
            return True
        else:
            packet.markAsDeflected()
            #print "packet",packet,"is marked as deflected"
            return False
    
    def sendDownward(self,degree,freeBelowEdges,output,packet):
        if len(freeBelowEdges)>0:
            level = self.switch.getLevel()
            position = self.switch.getPosition() 
            widthPod = degree**(level)
            #print "level",level,"position",position,"widthPod",widthPod, "packet",packet,"degree",degree
            pod = position / widthPod
            widthSubPod = widthPod/degree
            #for (a,n) in freeBelowEdges:
            #    print "op1",(int(n.name.split('_')[1])-(pod*widthPod))/widthSubPod,"op2",(int(packet.name.split('_')[1])-(pod*widthPod))/widthSubPod                              
            edges = filter(lambda (a,n): (n.position-(pod*widthPod))/widthSubPod== (packet.getDestination().position-(pod*widthPod))/widthSubPod,freeBelowEdges)
            if len(edges) == 0:
                packet.markAsDeflected()
                #print "packet",packet,"is marked as deflected"
                #print "##########output",output
                return False
            else:
                edge = edges[0]
                if not edge in output.keys():
                    output[edge]=deque([])
                if len(self.switch.getPorts()[edge]) +1 +len(output[edge]) == self.switch.getBufferSize(): 
                    freeBelowEdges.remove(edge)
                output[edge].append(packet)
                #print "packet",packet,"not marked as deflected",packet.isDeflected(), "output", output[edge], "---",output
                return True
        else:
            packet.markAsDeflected()
            #print "packet",packet,"is marked as deflected"
            #print "##########output",output
            return False
            