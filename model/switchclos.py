
import switch

''' 
    This class implements a switch in a ClosNetwork. Refers to 
    model.switch.Switch for more details.  
    - a switch is identified by its x and y cooridinates in the Clos Network.
      the left-bottm vertex is x=y=0. 
'''
class SwitchClos(switch.Switch):
    
#    def __init__(self,name,size):
#        switch.Switch.__init__(self, name, size)
    
    def __init__(self,level,position,size):
        switch.Switch.__init__(self, size)
        self.level=level
        self.position=position  
        self.destination2outgoingLinks = {}   
        
    def __eq__(self,other):
        if other != None:
            return self.level == other.level and self.position == other.position
        return False
            
    def __hash__(self):
        return (self.level,self.position).__hash__()  
    
    def __str__(self):
        return str(self.level)+'_'+str(self.position) 
    
    def __repr__(self):
        return 'S'+str(self.level)+'_'+str(self.position)  

    ''' return a list of edges that belongs to the upper level '''
    def getEdgesAboveLevel(self):
        return filter(lambda (a,n) : n.level == self.level+1,self.getPorts().keys())
    
    ''' return a list of edges that belongs to the lower level '''
    def getEdgesLowerLevel(self):
        return filter(lambda (a,n) : n.level == self.level-1,self.getPorts().keys())
    
    def getLevel(self):
        return self.level
    
    def getPosition(self):
        return self.position
    
    def isTopLevelSwitch(self):
        return self.getEdgesAboveLevel().__eq__([])
        
    ''' a speaker node in a Clos network is a bottom level switch '''
    def isSpeaker(self):
        return self.level == 0
    
    def getName(self):
        return str(self.level)+"_"+str(self.position)
    
    def getOutgoingLinksForDestination(self,destination):
             
        if not destination in self.destination2outgoingLinks.keys():
            level = self.getLevel()
            position = self.getPosition()
            if self.isTopLevelSwitch():
                degree = len(self.getPorts())
            else:
                degree = len(self.getPorts())/2 
            widthPod = degree**(level)
            pod = position / widthPod
            if pod == destination.position/widthPod:
                #is downwarddirected        
                widthSubPod = widthPod/degree
                edges = filter(lambda (a,n): (n.position-(pod*widthPod))/widthSubPod== (destination.position-(pod*widthPod))/widthSubPod,self.getEdgesLowerLevel())
                self.destination2outgoingLinks[destination]=edges
            else:
                self.destination2outgoingLinks[destination]=self.getEdgesAboveLevel()
        return self.destination2outgoingLinks[destination]
    
    def addEdge(self,neighbor,a):
        switch.Switch.addEdge(self, neighbor, a)
        
        
        
    