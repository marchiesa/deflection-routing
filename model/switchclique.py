
from collections import deque
import model.routingpolicyclique as rpc
import switch


''' 
    This class implements a Switch inside a Clique. Refers to model.switch.Switch. 
    - a switch is identified by a name. 
'''

class SwitchClique(switch.Switch):
   
    def __init__(self,size):
        switch.Switch.__init__(self, size)
        self.routingPolicy = rpc.RoutingPolicyClique(self)

    def setName(self,name):
        self.name=str(name)
        
    def getName(self):
        return self.name

    def __eq__(self,other):
        if other != None:
            return self.name.__eq__(other.name)
        return False
            
    def __hash__(self):
        return self.name.__hash__()    
        
    def __str__( self ):
        return self.name
    
    def __repr__(self):
        return "S%s"% self.__str__()
        
 