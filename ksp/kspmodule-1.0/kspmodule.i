%module kspmodule
%{
	const char* ksp(char *graph, int top_k);
	const char* dsp(char *graph);
	const char* ksp2file(char *graph, int top_k);
%}

const char* ksp(char *graph, int top_k);
const char* dsp(char *graph);
const char* ksp2file(char *graph, int top_k);
