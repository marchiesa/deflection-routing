/************************************************************************/
/* $Id: MainP.cpp 65 2010-09-08 06:48:36Z yan.qi.asu $                                                                 */
/************************************************************************/

#include <limits>
#include <set>
#include <map>
#include <queue>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include "GraphElements.h"
#include "Graph.h"
#include "DijkstraShortestPathAlg.h"
#include "YenTopKShortestPathsAlg.h"
#include <sstream>

using namespace std;


std::string DijkstraAlg(string graph)
{
	//Graph my_graph("../data/test_6_2");
	//Graph my_graph("data/danYen");
	Graph my_graph(graph);
	stringstream ostream;
	//Graph my_graph("2\n\n1 2 1\n");

	int nVertex = my_graph.get_nVertex();
	ostream.str("");

	for(int start = 0; start < nVertex; start++)
	{
		for(int end = 0; end < nVertex; end++)
		{
			if(end == start) continue;
			ostream << start << "," << end << endl;
			DijkstraShortestPathAlg dijkstraAlg(&my_graph);

			int count = 0;
			while(count < 1)
			{
				++count;
				dijkstraAlg.get_shortest_path(my_graph.get_vertex(start), my_graph.get_vertex(end))->PrintOut(ostream);
			}
			ostream << ";";
		}
	}
	
	return ostream.str();

// 	System.out.println("Result # :"+i);
// 	System.out.println("Candidate # :"+yenAlg.get_cadidate_size());
// 	System.out.println("All generated : "+yenAlg.get_generated_path_size());

}

std::string YenAlg(string graph, int top_k)
{
	//Graph my_graph("../data/test_6_2");
	//Graph my_graph("data/danYen");
	Graph my_graph(graph);
	stringstream ostream;
	//Graph my_graph("2\n\n1 2 1\n");

	int nVertex = my_graph.get_nVertex();
	ostream.str("");

	for(int start = 0; start < nVertex; start++)
	{
		cout << start << "/" << nVertex << endl;
		for(int end = 0; end < nVertex; end++)
		{
			if(end == start) continue;
			ostream << start << "," << end << endl;
			YenTopKShortestPathsAlg yenAlg(my_graph, my_graph.get_vertex(start),
				my_graph.get_vertex(end));

			int count = 0;
			while(yenAlg.has_next() && count < top_k)
			{
				++count;
				yenAlg.next()->PrintOut(ostream);
			}
			ostream << ";";
		}
	}
	
	return ostream.str();

// 	System.out.println("Result # :"+i);
// 	System.out.println("Candidate # :"+yenAlg.get_cadidate_size());
// 	System.out.println("All generated : "+yenAlg.get_generated_path_size());

}

const char *ksp(char *graph, int top_k)
{
	//cout << "Best Path!" << endl;
	//testDijkstraGraph();
	return YenAlg(graph, top_k).c_str();
	//testYenAlg(start, end);
}

const char *dsp(char *graph)
{
	return DijkstraAlg(graph).c_str();
}

const char *ksp2file(char *graph, int top_k)
{
	ofstream outputFile, graphFile;
	outputFile.open ("path.txt");
	graphFile.open("graph.txt");

	graphFile << graph;
	graphFile.close();

	string routing;
	routing = YenAlg(graph, 12);
	outputFile << routing;
	outputFile.close();
	return routing.c_str();
}


int main()
{
	
	ksp2file("4\n0 1 1\n0 2 1\n1 3 1\n2 3 1\n", 12);

}

/*
int main()
{
	
	//Graph my_graph;
	dsp("4\n0 1 1\n0 2 1\n1 3 1\n2 3 1\n");
	return 1;

	//cout << "Best Path!" << endl;
	//testDijkstraGraph();
	//cout << "K Path!" << endl;

}
*/

