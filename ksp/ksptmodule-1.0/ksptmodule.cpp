#include <set>
#include <map>
#include <queue>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <sstream>

using namespace std;
typedef int Vertex;
typedef set<Vertex> VertexSet;
typedef map<Vertex, VertexSet > Graph;
typedef queue< pair<Vertex, int> > BestPorts;

Graph construct_graph(const string& stringBuffer )
{
	Graph graph;
	//convert string buffer into StringStream
	//const char* file_name = input_file_name.c_str();
	stringstream ifs(stringstream::in | stringstream::out);
	ifs << stringBuffer;
	int m_nVertexNum;

	//1. Check the validity of the file
	//ifstream ifs(file_name);
	if (!ifs)
	{
		exit(1);
	}

	//3. Start to read information from the input file. 
	/// Note the format of the data in the graph file.
	//3.1 The first line has an integer as the number of vertices of the graph
	ifs >> m_nVertexNum;

	//3.2 In the following lines, each line contains a directed edge in the graph:
	///   the id of starting point, the id of ending point, the weight of the edge. 
	///   These values are separated by 'white space'. 
	int start_vertex, end_vertex;
	double edge_weight;

	while(ifs >> start_vertex)
	{
		if (start_vertex == -1)
		{
			break;
		}
		ifs >> end_vertex;
		ifs >> edge_weight;

		///3.2.1 construct the vertices
		if(graph.find(start_vertex) == graph.end())
		{
			set<int> vertexSet;
			vertexSet.insert(end_vertex);
			graph[start_vertex] = vertexSet;
		}
		else
		{
			set<int> vertexSet = graph[start_vertex];
			vertexSet.insert(end_vertex);
			graph[start_vertex] = vertexSet;
		}
	}

	cout << "graph size:" << graph.size() << endl;
	for (int i = 0; i < graph.size(); i++)
	{
		if(graph.find(i) == graph.end()) cout << "cannot find " << i << endl;
	}


	return graph;
}

void print_out_graph(Graph graph)
{
	Graph::iterator it_graph;
	VertexSet::iterator it_set;
	
	for (it_graph = graph.begin(); it_graph != graph.end(); ++it_graph)
	{
		cout << it_graph->first << endl;
		for (it_set = it_graph->second.begin(); it_set != it_graph->second.end(); ++it_set)
		{
			cout << *it_set << " ";
		}
		cout << endl <<endl;
	}
}

Graph graph_remove_vertex(Graph graph, Vertex vertex)
{
	//graph.erase(vertex);
	Graph::iterator it_graph;
	VertexSet::iterator it_set;
	VertexSet neightbors = graph[vertex];

	for (it_set = neightbors.begin(); it_set != neightbors.end(); ++it_set)
	{
		VertexSet vSet = graph[*it_set];
		vSet.erase(vertex);
		graph[*it_set] = vSet;
	}

	graph.erase(vertex);
	
	return graph;
}


BestPorts find_topk_path(Graph graph, Vertex source, Vertex target, VertexSet targetNeighbors, VertexSet unconnectedSet, int top_k)
{
	queue< pair<Vertex, int> > vertexQu;
	vertexQu.push(make_pair(source,0));
	unconnectedSet.erase(source);
	BestPorts set_topk;

	if(targetNeighbors.find(source) != targetNeighbors.end())
	{
		set_topk.push(make_pair(source, 0));
		targetNeighbors.erase(source);
	}

	unconnectedSet.erase(target);
	Vertex currentVertex;
	int weight;
	VertexSet currentNeighbors;
	VertexSet::iterator it;


	while(!vertexQu.empty() && !targetNeighbors.empty() && set_topk.size() < top_k)
	{
		pair<Vertex, int> vertexWeightPair;
		vertexWeightPair = vertexQu.front();
		vertexQu.pop();
		currentVertex = vertexWeightPair.first;
		weight = vertexWeightPair.second;

		currentNeighbors = graph[currentVertex];
		for(it = currentNeighbors.begin(); \
			it != currentNeighbors.end() && \
			!targetNeighbors.empty() && \
			set_topk.size() < top_k;\
			++it)
		{
			if(unconnectedSet.find(*it) != unconnectedSet.end())
			{
				vertexQu.push(make_pair(*it, weight+1));
				unconnectedSet.erase(*it);
				if(targetNeighbors.find(*it) != targetNeighbors.end())
				{
					set_topk.push(make_pair(*it, weight+1));
					targetNeighbors.erase(*it);
				}
			}
		}
	}

	return set_topk;
}

std::string find_topk_ports(string graph_text, int top_k)
{
	Graph graph = construct_graph(graph_text);
	VertexSet allSet;

	Graph::iterator it_graph, it_graph2;
	for (it_graph = graph.begin(); it_graph != graph.end(); ++it_graph)
	{
		allSet.insert(it_graph->first);
	}

	stringstream ostream;
	ostream.str("");

	for (it_graph = graph.begin(); it_graph != graph.end(); ++it_graph)
	{
		cout << it_graph->first << "/" << graph.size() << endl;
		for (it_graph2 = graph.begin(); it_graph2 != graph.end(); ++it_graph2)
		{
			Vertex source = it_graph->first;
			Vertex target = it_graph2->first;
			if(source == target) continue;
			VertexSet targetNeighbors = graph[target];
			Graph graph_tmp = graph_remove_vertex(graph, target);
			BestPorts bestPorts = find_topk_path(graph_tmp, source, target, targetNeighbors, allSet, top_k);

			ostream << source << "," << target << ";";
			while (!bestPorts.empty())
		  	{
		  		pair<Vertex, int> port = bestPorts.front();
		  		bestPorts.pop();
		    	ostream << port.first << "," <<port.second << ";";
		  	}
		  	ostream << endl;
		}
	 }
	return ostream.str();
}

const char *best_k_ports(char *graph, int top_k)
{
	cout << "reminder: must have edges in double direction" << endl;
	ofstream outputFile, graphFile;
	outputFile.open ("path.txt");
	graphFile.open("graph.txt");

	graphFile << graph;
	graphFile.close();

	string routing;
	routing = find_topk_ports(graph, top_k);
	outputFile << routing;
	outputFile.close();
	return routing.c_str();
}

/*
int main()
{
	best_k_ports("5\n0 1 1\n1 0 1\n0 2 1\n2 0 1\n1 2 1\n2 1 1\n1 3 1\n3 1 1\n2 3 1\n3 2 1\n2 4 1\n4 2 1\n3 4 1\n4 3 1\n", 12);
}
*/
